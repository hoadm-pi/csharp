﻿using System;

namespace P9
{
    class Program
    {
        /*
        * Bài tập 2 - Buổi 2 - Mảng 2 chiều (phần 2) - Trang 5
        * Nhân 2 ma trận
        * 
        * Ví dụ:
        * 
        * a =   2 2
        *       1 2
        *       3 4
        *       
        * b =   5 6 7
        *       8 9 10
        * 
        * 
        * c =   26 30 34
        *       21 24 27
        *       47 54 61
        */

        static int[,] InputMatrix(int rows, int cols)
        {
            int[,] arr = new int[rows, cols];

            for (int i = 0; i < rows; i++)
            {
                Console.Write("Row {0}: ", i);
                string[] tokens = Console.ReadLine().Split();
                for (int j = 0; j < cols; j++)
                {
                    arr[i, j] = int.Parse(tokens[j]);
                }
            }

            return arr;
        }

        static void Show(int[,] arr)
        {
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    Console.Write("{0, -3} ", arr[i, j]);
                }
                Console.WriteLine();
            }
        }

        static int DotProduct(int[,] matrixA, int[,] matrixB, int rowA, int colB)
        {
            int result = 0;

            for (int i = 0; i < matrixA.GetLength(1); i++)
            {
                result += matrixA[rowA, i] * matrixB[i, colB];
            }    

            return result;
        }

        static int[,] ProductMatrix(int[,] matrixA, int[,] matrixB)
        {
            int rows = matrixA.GetLength(0);
            int cols = matrixB.GetLength(1);

            int[,] result = new int[rows, cols];

            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    result[i, j] = DotProduct(matrixA, matrixB, i, j);   
                }    
            }

            return result;
        }


        static void Main(string[] args)
        {
            Console.Write("Input m, n, p: ");
            string[] tokens = Console.ReadLine().Split();
            int m = int.Parse(tokens[0]);
            int n = int.Parse(tokens[1]);
            int p = int.Parse(tokens[2]);

            Console.WriteLine("Input matrix A");
            int[,] matrixA = InputMatrix(m, n);
            Console.WriteLine("--------------");

            Console.WriteLine("Input matrix B");
            int[,] matrixB = InputMatrix(n, p);
            Console.WriteLine("--------------");

            int[,] result = ProductMatrix(matrixA, matrixB);

            Console.WriteLine("Matrix A: ");
            Show(matrixA);
            Console.WriteLine("--------------");

            Console.WriteLine("Matrix B: ");
            Show(matrixB);
            Console.WriteLine("--------------");

            Console.WriteLine("A x B = ");
            Show(result);
            Console.WriteLine("--------------");

        }
    }
}
