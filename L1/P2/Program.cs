﻿using System;

namespace P2
{
    class Program
    {
        /*
        * Bài tập 2 - Buổi 1 - Mảng 2 chiều (phần 1) - Trang 2
        * Viết chương trình nhập một bảng số nguyên a[mxn], (1 ≤ m, n ≤ 100) 
        * và một số nguyên k (0 ≤ k < m). In ra màn hình các phần tử nằm 
        * trên dòng thứ k của bảng a(các phần tử nằm trên cùng một dòng và 
        * cách nhau bởi ký tự khoảng trắng). Trường hợp giá trị k không hợp lệ,
        * in thông báo “Invalid k”.
        * 
        * Input: 
        * - Dòng đầu tiên: 3 số nguyên m, n, k
        * - m dòng tiếp theo: mỗi dòng chứa n số nguyên cách nhau bởi dấu 
        * khoảng trắng là giá trị các phần tử của bảng số nguyên a.
        * 
        * Output:
        * 1 dòng duy nhất chứa các phần tử trên dòng thứ k của bảng a hoặc 
        * thông báo “Invalid k”
        * 
        * Input:
        * 3 4 1
        * 4 5 6 4
        * 9 7 2 8
        * 2 5 7 3
        * 
        * Output:
        * 9 7 2 8
        */

        static int[,] Input2DArray(int rows, int cols)
        {
            int[,] arr = new int[rows, cols];

            for (int i = 0; i < rows; i++)
            {
                Console.Write("Row {0}: ", i);
                string[] tokens = Console.ReadLine().Split();
                for (int j = 0; j < cols; j++)
                {
                    arr[i, j] = int.Parse(tokens[j]);
                }
            }

            return arr;
        }

        static void PrintRow(int[,] arr, int row)
        {
            if (row < 0 || row >= arr.GetLength(0))
            {
                Console.WriteLine("invalid k!");
            }
            else
            {
                for (int i = 0; i < arr.GetLength(1); i++)
                {
                    Console.Write("{0} ", arr[row, i]);
                }    
            }
        }

        static void Main(string[] args)
        {
            Console.Write("Input: ");
            string[] tokens = Console.ReadLine().Split();

            int rows = int.Parse(tokens[0]);
            int cols = int.Parse(tokens[1]);
            int r = int.Parse(tokens[2]);

            int[,] arr = Input2DArray(rows, cols);
            PrintRow(arr, r);
        }
    }
}
