﻿using System;

namespace P8
{
    class Program
    {
        /*
        * Bài tập 2 - Buổi 2 - Mảng 2 chiều (phần 2) - Trang 4
        * Cộng 2 ma trận
        * 
        * Cho hai ma trận số nguyên a[m x n], b[m x n] với (1 <= m, n, p, <= 100)
        * phép cộng 2 ma trận a và b được ma trận c[m x n] có các phần tử c[i, j]
        * c[i, j] = a[i, j] + b[i, j]
        * 
        * Ví dụ:
        * 
        * a =   2 3 4
        *       5 6 7
        *       
        * b =   4 5 6
        *       7 8 9
        * 
        * 
        * c =   6   8   10
        *       12  14  16
        */

        static int[,] InputMatrix(int rows, int cols)
        {
            int[,] arr = new int[rows, cols];

            for (int i = 0; i < rows; i++)
            {
                Console.Write("Row {0}: ", i);
                string[] tokens = Console.ReadLine().Split();
                for (int j = 0; j < cols; j++)
                {
                    arr[i, j] = int.Parse(tokens[j]);
                }
            }

            return arr;
        }

        static void Show(int[,] arr)
        {
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    Console.Write("{0, -3} ", arr[i, j]);
                }
                Console.WriteLine();
            }
        }

        static int[,] Sum(int[,] matrixA, int[,] matrixB)
        {
            int rows = matrixA.GetLength(0);
            int cols = matrixA.GetLength(1);
            int[,] result = new int[rows, cols];

            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    result[i, j] = matrixA[i, j] + matrixB[i, j];
                }    
            }

            return result;
        }



        static void Main(string[] args)
        {
            Console.Write("Rows and cols: ");
            string[] tokens = Console.ReadLine().Split();
            int rows = int.Parse(tokens[0]);
            int cols = int.Parse(tokens[1]);


            Console.WriteLine("Input matrix A");
            int[,] matrixA = InputMatrix(rows, cols);
            Console.WriteLine("--------------");

            Console.WriteLine("Input matrix B");
            int[,] matrixB = InputMatrix(rows, cols);
            Console.WriteLine("--------------");


            int[,] result = Sum(matrixA, matrixB);

            Console.WriteLine("Matrix A: ");
            Show(matrixA);
            Console.WriteLine("--------------");

            Console.WriteLine("Matrix B: ");
            Show(matrixB);
            Console.WriteLine("--------------");

            Console.WriteLine("A + B = ");
            Show(result);
            Console.WriteLine("--------------");
        }
    }
}
