﻿using System;

namespace P10
{
    class Program
    {
        /*
        * Bài tập 3 - Buổi 2 - Mảng 2 chiều (phần 2) - Trang 6
        * Nhân 2 ma trận
        * 
        * Input:
        * 3 7
        * 3 4 2
        * 
        * 34  12  15  11  10  2   15  
        * 56  32  30  40  23  33  24  
        * 6   13  12  20  15  19  8 
        * 
        * Output:
        * 
        * 1435
        * 338 190 189 233 152 176 157
        */

        static int[,] Input2DArray(int rows, int cols)
        {
            int[,] arr = new int[rows, cols];

            for (int i = 0; i < rows; i++)
            {
                Console.Write("Day {0}: ", i);
                string[] tokens = Console.ReadLine().Split();
                for (int j = 0; j < cols; j++)
                {
                    arr[i, j] = int.Parse(tokens[j]);
                }
            }

            return arr;
        }

        static int[] InputArray(int n)
        {
            int[] arr = new int[n];
            string[] tokens = Console.ReadLine().Split();

            for (int i = 0; i < n; i++)
            {
                arr[i] = int.Parse(tokens[i]);
            }    

            return arr;
        }

        static void Show(int[] arr)
        {
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                Console.Write("{0, -3} ", arr[i]);
            }
            Console.WriteLine();
        }

        static void Show(int[,] arr)
        {
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    Console.Write("{0, -3} ", arr[i, j]);
                }
                Console.WriteLine();
            }
        }

        static int[] DailyRevenue(int[] prices, int[,] sales)
        {
            int days = sales.GetLength(1);
            int[] results = new int[days];

            for (int i = 0; i < sales.GetLength(1); i++)
            {
                int revenue = 0;
                for (int j = 0; j < prices.GetLength(0); j++)
                {
                    revenue += prices[j] * sales[j, i];
                }

                results[i] = revenue;
            }

            return results;
        }

        static int TotalRevenue(int[] revenues)
        {
            int total = 0;

            for (int i = 0; i < revenues.GetLength(0); i++)
            {
                total += revenues[i];
            }

            return total;
        }

        static void Main(string[] args)
        {
            Console.Write("Input m, n: ");
            string[] tokens = Console.ReadLine().Split();
            int m = int.Parse(tokens[0]);
            int n = int.Parse(tokens[1]);

            Console.Write("Input prices: ");
            int[] prices = InputArray(m);

            Console.WriteLine("Input sales: ");
            int[,] sales = Input2DArray(m, n);

            Console.WriteLine("--------------");
            Console.Write("Prices: ");
            Show(prices);
            Console.WriteLine("--------------");

            Console.WriteLine("Sales: ");
            Show(sales);
            Console.WriteLine("--------------");

            Console.WriteLine("Output: ");
            int[] revenues = DailyRevenue(prices, sales);
            Console.WriteLine("{0}", TotalRevenue(revenues));
            Console.WriteLine("Daily Revenue:");
            Show(revenues);
        }
    }
}
