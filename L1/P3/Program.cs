﻿using System;

namespace P3
{
    class Program
    {
        /*
        * Bài tập 3 - Buổi 1 - Mảng 2 chiều (phần 1) - Trang 2
        * Viết chương trình nhập một bảng số nguyên a[mxn], (1 ≤ m, n ≤ 100) 
        * và một số nguyên k (0 ≤ k < n). In ra màn hình các phần tử nằm 
        * trên cột thứ k của bảng a (mỗi phần tử nằm trên 1 dòng). Trường hợp giá trị k không hợp lệ,
        * in thông báo “Invalid k”.
        * 
        * Input: 
        * - Dòng đầu tiên: 3 số nguyên m, n, k
        * - m dòng tiếp theo: mỗi dòng chứa n số nguyên cách nhau bởi dấu 
        * khoảng trắng là giá trị các phần tử của bảng số nguyên a.
        * 
        * Output:
        * m dòng chứa các phần tử trên cột thứ k của bảng a hoặc 
        * thông báo “Invalid k”
        * 
        * Input:
        * 3 4 1
        * 4 5 6 4
        * 9 7 2 8
        * 2 5 7 3
        * 
        * Output:
        * 5
        * 7
        * 5
        */

        static int[,] Input2DArray(int rows, int cols)
        {
            int[,] arr = new int[rows, cols];

            for (int i = 0; i < rows; i++)
            {
                Console.Write("Row {0}: ", i);
                string[] tokens = Console.ReadLine().Split();
                for (int j = 0; j < cols; j++)
                {
                    arr[i, j] = int.Parse(tokens[j]);
                }
            }

            return arr;
        }

        static void PrintCol(int[,] arr, int col)
        {
            if (col < 0 || col >= arr.GetLength(1))
            {
                Console.WriteLine("invalid k!");
            }
            else
            {
                for (int i = 0; i < arr.GetLength(0); i++)
                {
                    Console.WriteLine("{0} ", arr[i, col]);
                }
            }
        }

        static void Main(string[] args)
        {
            Console.Write("Input: ");
            string[] tokens = Console.ReadLine().Split();

            int rows = int.Parse(tokens[0]);
            int cols = int.Parse(tokens[1]);
            int c = int.Parse(tokens[2]);

            int[,] arr = Input2DArray(rows, cols);
            PrintCol(arr, c);
        }
    }
}
