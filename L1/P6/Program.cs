﻿using System;

namespace P6
{
    class Program
    {
        /*
        * Bài tập 6 - Buổi 1 - Mảng 2 chiều (phần 1) - Trang 3
        * Sắp xếp trên mảng 2 chiều
        * Cho một bảng số nguyên a[mxn] (1 ≤ m, n ≤  100). Định nghĩa các hàm 
        * chức năng sau:
        * 
        * a) Sắp xếp các phần tử trên dòng thứ k của bảng a theo thứ tự 
        * tăng dần, với k là tham số của hàm.
        * 
        * b) Sắp xếp các phần tử trên cột thứ k của bảng a theo thứ tự 
        * giảm dần, với k là tham số của hàm.
        * 
        * c) Đổi chỗ 2 hàng của bảng a,chỉ số của 2 hàng cần đổi chỗ là 
        * tham số của hàm.
        * 
        * d) Sắp xếp bảng a sao cho tổng của từng dòng tăng dần từ trên
        * xuống dưới (dòng đầu tiên cósau khi sắp xếp, tổng các phần tử trên
        * dòng đầu tiên là nhỏ nhất, tổng các phần tử trên dòng cuối cùng
        * là lớn nhất)
        */

        // Input & Show 2D Array
        static int[,] Input2DArray(int rows, int cols)
        {
            int[,] arr = new int[rows, cols];

            for (int i = 0; i < rows; i++)
            {
                Console.Write("Row {0}: ", i);
                string[] tokens = Console.ReadLine().Split();
                for (int j = 0; j < cols; j++)
                {
                    arr[i, j] = int.Parse(tokens[j]);
                }
            }

            return arr;
        }

        static void Show(int[,] arr)
        {
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    Console.Write("{0} ", arr[i, j]);
                }
                Console.WriteLine();
            }
        }


        // a - buble sort
        static void SortRow(int[,] arr, int row)
        {
            for (int i = 0; i < arr.GetLength(1) - 1; i++)
            {
                for (int j = i + 1; j < arr.GetLength(1); j++)
                {
                    if (arr[row, j] < arr[row, i])
                    {
                        // swap
                        int tmp = arr[row, i];
                        arr[row, i] = arr[row, j];
                        arr[row, j] = tmp;
                    }    
                }    
            }    
        }

        // b
        static void SortCol(int[,] arr, int col)
        {
            for (int i = 0; i < arr.GetLength(0) - 1; i++)
            {
                for (int j = i + 1; j < arr.GetLength(0); j++)
                {
                    if (arr[j, col] > arr[i, col])
                    {
                        // swap
                        int tmp = arr[i, col];
                        arr[i, col] = arr[j, col];
                        arr[j, col] = tmp;
                    }
                }
            }
        }

        // c
        static void SwapRow(int[,] arr, int row1, int row2)
        {
            for (int i = 0; i < arr.GetLength(1); i++)
            {
                // swap
                int tmp = arr[row1, i];
                arr[row1, i] = arr[row2, i];
                arr[row2, i] = tmp;
            }    
        }

        // d

        static int SumOfRow(int[,] arr, int row)
        {
            int total = 0;

            for (int i = 0; i < arr.GetLength(1); i++)
            {
                total += arr[row, i];
            }

            return total;
        }

        static void SortArrByRowValue(int[,] arr)
        {
            for (int i = 0; i < arr.GetLength(0) - 1; i++)
            {
                for (int j = i + 1; j < arr.GetLength(0); j++)
                {
                    if (SumOfRow(arr, j) > SumOfRow(arr, i))
                    {
                        SwapRow(arr, i, j);
                    }
                }    
            }    
        }

        static void Main(string[] args)
        {
            Console.Write("Input: ");
            string[] tokens = Console.ReadLine().Split();

            int rows = int.Parse(tokens[0]);
            int cols = int.Parse(tokens[1]);

            int[,] arr = Input2DArray(rows, cols);
            Console.WriteLine("Input array: ");
            Show(arr);
            Console.WriteLine("----------------------");

            // a
            int row = 1;
            Console.WriteLine("Array is sorted row {0}", row);
            SortRow(arr, row);
            Show(arr);
            Console.WriteLine("----------------------");

            // b
            int col = 1;
            Console.WriteLine("Array is sorted col {0}", col);
            SortCol(arr, col);
            Show(arr);
            Console.WriteLine("----------------------");

            // c
            int row1 = 0;
            int row2 = 1;
            Console.WriteLine("Array after swap row {0} and row {1}", row1, row2);
            SwapRow(arr, row1, row2);
            Show(arr);
            Console.WriteLine("----------------------");

            // d
            Console.WriteLine("Sorted array:");
            SortArrByRowValue(arr);
            Show(arr);
        }
    }
}
