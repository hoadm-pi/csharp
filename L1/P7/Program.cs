﻿using System;

namespace P7
{
    class Program
    {
        /*
        * Bài tập 7 - Buổi 1 - Mảng 2 chiều (phần 1) - Trang 3
        * Sắp xếp trên mảng 2 chiều
        * Cho một bảng số nguyên a[mxn] (1 ≤ m, n ≤  100). Định nghĩa các hàm 
        * chức năng sau:
        * 
        * a) In ra màn hình các phần tử nằm trên đường chéo chính
        * 
        * b) In ra màn hình các phần tử nằm trên đường chéo phụ
        * 
        * c) Đổi chỗ 2 hàng của bảng a,chỉ số của 2 hàng cần đổi chỗ là 
        * tham số của hàm.
        * 
        * d) In ra màn hình các phần tử thuộc tam giác trên đường chéo chính
        * 
        * Giống các bài tập trên lớp E8, E9, E10. 
        * Cập nhật: 
        * - đặt lại tên các functions (2DArray -> Matrix). 
        * - in lại tam giác trên đẹp hơn
        * 
        * Input:
        * 3
        * 5 1 3
        * 4 7 2
        * 4 2 6
        */

        static int[,] InputMatrix(int rows, int cols)
        {
            int[,] arr = new int[rows, cols];

            for (int i = 0; i < rows; i++)
            {
                Console.Write("Row {0}: ", i);
                string[] tokens = Console.ReadLine().Split();
                for (int j = 0; j < cols; j++)
                {
                    arr[i, j] = int.Parse(tokens[j]);
                }
            }

            return arr;
        }

        static void PrintMainDiagonal(int[,] matrix)
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (i == j)
                    {
                        Console.WriteLine("{0}", matrix[i, j]);
                    }
                }
            }
        }

        static void PrintAntiDiagonal(int[,] matrix)
        {
            int a = matrix.GetLength(0) - 1;

            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (i + j == a)
                    {
                        Console.WriteLine("{0}", matrix[i, j]);
                    }
                }
            }
        }

        static void PrintUpperTriangularMatrix(int[,] matrix)
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (i <= j)
                    {
                        Console.Write("{0} ", matrix[i, j]);
                    } else
                    {
                        Console.Write("  ");
                    }
                }
                Console.WriteLine();
            }
        }


        static void Main(string[] args)
        {
            Console.Write("Input: ");
            int rows = int.Parse(Console.ReadLine());
            int cols = rows;

            int[,] matrix = InputMatrix(rows, cols);

            Console.WriteLine("Main diagonal:");
            PrintMainDiagonal(matrix);
            Console.WriteLine("---------------");


            Console.WriteLine("Anti diagonal:");
            PrintAntiDiagonal(matrix);
            Console.WriteLine("---------------");

            Console.WriteLine("Upper Triangular Matrix:");
            PrintUpperTriangularMatrix(matrix);
            Console.WriteLine("---------------");
        }
    }
}
