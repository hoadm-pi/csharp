﻿using System;

namespace P13
{
    class Program
    {
        /*
        * Bài tập 6 - Buổi 2 - Mảng 2 chiều (phần 2) - Trang 9
        * Dot product của hai ma trận
        * 
        * A =   2 1 2
        *       1 1 1
        *       4 5 6
        *  
        * B =   4 5 6
        *       2 3 4
        *       1 2 3
        * 
        * A.B = 66
        *
        */

        static int[,] InputMatrix(int rows, int cols)
        {
            int[,] arr = new int[rows, cols];

            for (int i = 0; i < rows; i++)
            {
                Console.Write("Row {0}: ", i);
                string[] tokens = Console.ReadLine().Split();
                for (int j = 0; j < cols; j++)
                {
                    arr[i, j] = int.Parse(tokens[j]);
                }
            }

            return arr;
        }

        static void Show(int[,] arr)
        {
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    Console.Write("{0, -3} ", arr[i, j]);
                }
                Console.WriteLine();
            }
        }


        static int DotProduct(int[,] matrixA, int[,] matrixB)
        {
            int result = 0;

            for (int i = 0; i < matrixA.GetLength(0); i++)
            {
                for (int j = 0; j < matrixA.GetLength(1); j++)
                {
                    result += (matrixA[i, j] * matrixB[i, j]);
                }
            }

            return result;
        }

        static void Main(string[] args)
        {
            Console.Write("Input rows, cols: ");
            string[] tokens = Console.ReadLine().Split();
            int rows = int.Parse(tokens[0]);
            int cols = int.Parse(tokens[1]);

            Console.WriteLine("Input matrix A");
            int[,] matrixA = InputMatrix(rows, cols);
            Console.WriteLine("--------------");

            Console.WriteLine("Input matrix B");
            int[,] matrixB = InputMatrix(rows, cols);
            Console.WriteLine("--------------");


            Console.WriteLine("Matrix A: ");
            Show(matrixA);
            Console.WriteLine("--------------");

            Console.WriteLine("Matrix B: ");
            Show(matrixB);
            Console.WriteLine("--------------");

            Console.WriteLine("A.B = {0}", DotProduct(matrixA, matrixB));
        }
    }
}
