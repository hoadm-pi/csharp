﻿using System;

namespace P4
{
    class Program
    {
        /*
        * Bài tập 4 - Buổi 1 - Mảng 2 chiều (phần 1) - Trang 2
        * Tính toán cơ bản trên mảng 2 chiều
        * Cho một bảng số nguyên a[m×n], (1 ≤ m, n ≤ 100). Định nghĩa các 
        * hàm chức năng sau: 
        * a) Tính tổng các phần tử nằm trên dòng thứ k của bảng a, với k 
        * là tham số của hàm.
        * b) Tính tổng các phần tử nằm trên cột thứ k của bảng a, với k 
        * là tham số của hàm.
        * c) Tính tổng tất cả các phần tử trong bảng a.
        * d) Tính tổng các phần tử là số chẵn trong bảng a.
        * e) Tính tổng các phần tử là số lé trong bảng a.
        * f) Tính giá trị trung bình của tất cả các phần tử trong bảng a.
        */


        // Input & Show 2D Array
        static int[,] Input2DArray(int rows, int cols)
        {
            int[,] arr = new int[rows, cols];

            for (int i = 0; i < rows; i++)
            {
                Console.Write("Row {0}: ", i);
                string[] tokens = Console.ReadLine().Split();
                for (int j = 0; j < cols; j++)
                {
                    arr[i, j] = int.Parse(tokens[j]);
                }
            }

            return arr;
        }

        static void Show(int[,] arr)
        {
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    Console.Write("{0} ", arr[i, j]);
                }
                Console.WriteLine();
            }
        }

        // a
        static int SumRow(int[,] arr, int row)
        {
            int total = 0;
            for (int i = 0; i < arr.GetLength(1); i++)
            {
                total += arr[row, i];
            }

            return total;
        }

        // b
        static int SumCol(int[,] arr, int col)
        {
            int total = 0;
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                total += arr[i, col];
            }

            return total;
        }


        // c
        static int Sum(int[,] arr)
        {
            int total = 0;

            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    total += arr[i, j];
                }
            }

            return total;
        }


        // d
        static int SumEvenElements(int[,] arr)
        {
            int total = 0;

            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    if (arr[i, j] % 2 == 0)
                    {
                        total += arr[i, j];
                    }    
                }
            }

            return total;
        }


        // e
        static int SumOddElements(int[,] arr)
        {
            int total = 0;

            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    if (arr[i, j] % 2 != 0)
                    {
                        total += arr[i, j];
                    }
                }
            }

            return total;
        }

        // f
        static float Average(int[,] arr)
        {
            float total = 0;

            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    total += arr[i, j];
                }
            }

            return total / arr.Length;
        }


        static void Main(string[] args)
        {
            Console.Write("Input: ");
            string[] tokens = Console.ReadLine().Split();

            int rows = int.Parse(tokens[0]);
            int cols = int.Parse(tokens[1]);

            int[,] arr = Input2DArray(rows, cols);
            Console.WriteLine("Array: ");
            Show(arr);

            int r = 0;
            int c = 2;

            Console.WriteLine("Sum of elements on row {0} = {1}", r, SumRow(arr, r));
            Console.WriteLine("Sum of elements on col {0} = {1}", c, SumCol(arr, c));
            Console.WriteLine("Sum of all elements: {0}", Sum(arr));
            Console.WriteLine("Sum of all even elements: {0}", SumEvenElements(arr));
            Console.WriteLine("Sum of all odd elements: {0}", SumOddElements(arr));
            Console.WriteLine("Average: {0}", Average(arr));
        }
    }
}
