﻿using System;

namespace P12
{
    class Program
    {
        /*
        * Bài tập 5 - Buổi 2 - Mảng 2 chiều (phần 2) - Trang 8
        * Khoảng cách Euclid giữa 2 ma trận
        * 
        * a =  4 5
        *      6 7
        *  
        * b =  2 5
        *      8 1
        * 
        * distance = 6.63
        *
        */

        static int[,] InputMatrix(int rows, int cols)
        {
            int[,] arr = new int[rows, cols];

            for (int i = 0; i < rows; i++)
            {
                Console.Write("Row {0}: ", i);
                string[] tokens = Console.ReadLine().Split();
                for (int j = 0; j < cols; j++)
                {
                    arr[i, j] = int.Parse(tokens[j]);
                }
            }

            return arr;
        }

        static void Show(int[,] arr)
        {
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    Console.Write("{0, -3} ", arr[i, j]);
                }
                Console.WriteLine();
            }
        }


        static double Distance(int[,] matrixA, int[,] matrixB)
        {
            double diff = 0;

            for (int i = 0; i < matrixA.GetLength(0); i++)
            {
                for (int j = 0; j < matrixA.GetLength(1); j++)
                {
                    diff += Math.Pow((matrixA[i, j] - matrixB[i, j]), 2);
                }    
            }    

            return Math.Sqrt(diff);
        }

        static void Main(string[] args)
        {
            Console.Write("Input rows, cols: ");
            string[] tokens = Console.ReadLine().Split();
            int rows = int.Parse(tokens[0]);
            int cols = int.Parse(tokens[1]);


            Console.WriteLine("Input matrix A");
            int[,] matrixA = InputMatrix(rows, cols);
            Console.WriteLine("--------------");

            Console.WriteLine("Input matrix B");
            int[,] matrixB = InputMatrix(rows, cols);
            Console.WriteLine("--------------");

            Console.Write("Distance between A and B: {0:F2}", Distance(matrixA, matrixB));

        }
    }
}
