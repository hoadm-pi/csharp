﻿using System;

namespace P5
{
    class Program
    {
        /*
        * Bài tập 5 - Buổi 1 - Mảng 2 chiều (phần 1) - Trang 3
        * Tìm kiếm trên mảng 2 chiều
        * Cho một bảng số nguyên a[mxn] (1 ≤ m, n ≤  100). Định nghĩa các hàm 
        * chức năng sau:
        * 
        * a) Tìm giá trị phần tử lớn nhất trên dòng thứ k của bảng a,
        * với k là tham số của hàm.
        * b) Tìm giá trị phần tử nhỏ nhất trên cột thứ k của bảng a, 
        * với k là tham số của hàm.
        * c) Tìm tất cả phần tử là số nguyên tố trong bảng a.
        */

        // Input & Show 2D Array
        static int[,] Input2DArray(int rows, int cols)
        {
            int[,] arr = new int[rows, cols];

            for (int i = 0; i < rows; i++)
            {
                Console.Write("Row {0}: ", i);
                string[] tokens = Console.ReadLine().Split();
                for (int j = 0; j < cols; j++)
                {
                    arr[i, j] = int.Parse(tokens[j]);
                }
            }

            return arr;
        }

        static void Show(int[,] arr)
        {
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    Console.Write("{0} ", arr[i, j]);
                }
                Console.WriteLine();
            }
        }

        // a
        static int MaxOnRow(int[,] arr, int row)
        {
            int max = arr[row, 0];

            for (int i = 1; i < arr.GetLength(1); i++)
            {
                if (arr[row, i] > max)
                {
                    max = arr[row, i];
                }    
            }

            return max;
        }

        // b
        static int MinOnCol(int[,] arr, int col)
        {
            int min = arr[0, col];

            for (int i = 1; i < arr.GetLength(0); i++)
            {
                if (arr[i, col] < min)
                {
                    min = arr[i, col];
                }
            }

            return min;
        }

        // c
        static bool IsPrime(int n)
        {
            if (n < 2)
            {
                return false;
            }

            for (int i = 2; i <= Math.Sqrt(n); i++)
            {
                if (n % i == 0)
                {
                    return false;
                }
            }

            return true;
        }

        static void PrintPrimeElements(int[,] arr)
        {
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    if (IsPrime(arr[i, j]))
                    {
                        Console.WriteLine("arr[{0},{1}] =  {2}", i, j, arr[i, j]);
                    }
                }    
            }    
        }

        static void Main(string[] args)
        {
            Console.Write("Input: ");
            string[] tokens = Console.ReadLine().Split();

            int rows = int.Parse(tokens[0]);
            int cols = int.Parse(tokens[1]);

            int[,] arr = Input2DArray(rows, cols);
            Console.WriteLine("Array: ");
            Show(arr);

            int r = 0;
            int c = 1;

            Console.WriteLine("Max value of row {0} is {1}", r, MaxOnRow(arr, r));
            Console.WriteLine("Min value of col {0} is {1}", c, MinOnCol(arr, c));
            Console.WriteLine("Prime elemens on array are: ");
            PrintPrimeElements(arr);
        }
    }
}
