﻿using System;

namespace P11
{
    class Program
    {
        /*
        * Bài tập 4 - Buổi 2 - Mảng 2 chiều (phần 2) - Trang 7
        * Chuyển vị ma trận
        * 
        * Ví dụ:
        * 
        * A =   5   6   7
        *       8   9   10
        *       
        * A^T = 5   8
        *       6   9
        *       7   10
        * 
        */

        static int[,] InputMatrix(int rows, int cols)
        {
            int[,] arr = new int[rows, cols];

            for (int i = 0; i < rows; i++)
            {
                Console.Write("Row {0}: ", i);
                string[] tokens = Console.ReadLine().Split();
                for (int j = 0; j < cols; j++)
                {
                    arr[i, j] = int.Parse(tokens[j]);
                }
            }

            return arr;
        }

        static void Show(int[,] arr)
        {
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    Console.Write("{0, -3} ", arr[i, j]);
                }
                Console.WriteLine();
            }
        }

        static int[,] Transpose(int[,] matrix)
        {
            int[,] result = new int[matrix.GetLength(1), matrix.GetLength(0)];

            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    result[j, i] = matrix[i, j];
                }    
            }    


            return result;
        }

        static void Main(string[] args)
        {
            Console.Write("Input rows, cols:  ");
            string[] tokens = Console.ReadLine().Split();
            int rows = int.Parse(tokens[0]);
            int cols = int.Parse(tokens[1]);

            Console.WriteLine("Input matrix A");
            int[,] matrixA = InputMatrix(rows, cols);
            Console.WriteLine("--------------");

            Console.WriteLine("Matrix A: ");
            Show(matrixA);
            Console.WriteLine("--------------");

            Console.WriteLine("Matrix A^T: ");
            Show(Transpose(matrixA));
            Console.WriteLine("--------------");
        }
    }
}
