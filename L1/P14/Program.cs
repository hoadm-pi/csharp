﻿using System;

namespace P14
{
    class Program
    {
        /*
        * Bài tập 7 - Buổi 2 - Mảng 2 chiều (phần 2) - Trang 10
        * Phép tích chập (Convolution)
        * 
        * k, n, k = 4 4 3
        * 
        * A =   4 2 2 4
        *       1 9 5 3
        *       1 4 2 4
        *       0 9 8 1
        *  
        * B =   -1 -1 -1
        *       -1  8 -1
        *       -1 -1 -1
        * 
        * C =   51  10
        *       -3 -27
        */

        static int[,] InputMatrix(int rows, int cols)
        {
            int[,] arr = new int[rows, cols];

            for (int i = 0; i < rows; i++)
            {
                Console.Write("Row {0}: ", i);
                string[] tokens = Console.ReadLine().Split();
                for (int j = 0; j < cols; j++)
                {
                    arr[i, j] = int.Parse(tokens[j]);
                }
            }

            return arr;
        }

        static void Show(int[,] arr)
        {
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    Console.Write("{0, -3} ", arr[i, j]);
                }
                Console.WriteLine();
            }
        }


        static int[,] Convolution(int[,] image, int[,] kernel)
        {
            int rows = image.GetLength(0) - kernel.GetLength(0) + 1;
            int cols = image.GetLength(1) - kernel.GetLength(1) + 1;
            int[,] conv = new int[rows, cols];

            for (int wi = 0; wi < rows; wi++)
            {
                for (int wj = 0; wj < cols; wj++)
                {
                    // Dot product
                    int el = 0;
                    for (int i = 0; i < kernel.GetLength(0); i++)
                    {
                        for (int j = 0; j < kernel.GetLength(1); j++)
                        {
                            el += image[wi + i, wj + j] * kernel[i, j];
                        }    
                    }
                    conv[wi, wj] = el;
                }
            }    

            return conv;
        }

        static void Main(string[] args)
        {
            Console.Write("Input m, n, k: ");
            string[] tokens = Console.ReadLine().Split();
            int m = int.Parse(tokens[0]);
            int n = int.Parse(tokens[1]);
            int k = int.Parse(tokens[2]);

            Console.WriteLine("Input matrix A");
            int[,] matrixA = InputMatrix(m, n);
            Console.WriteLine("--------------");

            Console.WriteLine("Input matrix B");
            int[,] matrixB = InputMatrix(k, k);
            Console.WriteLine("--------------");

            int[,] convolve = Convolution(matrixA, matrixB);

            Console.WriteLine("Matrix A: ");
            Show(matrixA);
            Console.WriteLine("--------------");

            Console.WriteLine("Matrix B: ");
            Show(matrixB);
            Console.WriteLine("--------------");

            Console.WriteLine("Convolution");
            Show(convolve);
        }
    }
}
