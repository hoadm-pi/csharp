namespace PT;

public class MyTime
{
    private int _hour;
    private int _minute;
    private int _second;

    public MyTime()
    {
        _hour = 0;
        _minute = 0;
        _second = 0;
    }

    public MyTime(int hour, int minute, int second)
    {
        _hour = hour;
        _minute = minute;
        _second = second;
    }

    public int Hour
    {
        get => _hour;
        set => _hour = (value is < 0 or > 23) ? 0 : value;
    }

    public int Minute
    {
        get => _minute;
        set => _minute = (value is < 0 or > 59) ? 0 : value;
    }
    
    public int Second
    {
        get => _second;
        set => _second = (value is < 0 or > 59) ? 0 : value;
    }

    public override string ToString()
    {
        return $"{_hour:00}:{_minute:00}:{_second:00}";
    }

    public void Print()
    {
        string abbr = _hour < 12 ? "AM" : "PM";
        int hour = _hour;
        
        if (_hour > 12)
        {
            hour %= 12;
        }

        Console.WriteLine($"{hour:00}:{_minute:00}:{_second:00} {abbr}");
    }

    public void AddHour(int hours)
    {
        _hour += hours;
        if (_hour > 23)
        {
            _hour %= 24;
        } 
    }
    
    public void AddMinute(int minutes)
    {
        int hours = (_minute + minutes) / 60;
        _minute = (_minute + minutes) % 60;
        AddHour(hours);
    }

    public void AddSecond(int seconds)
    {
        int minutes = (_second + seconds) / 60;
        _second = (_second + seconds) % 60;
        AddMinute(minutes);
    }
    
    public static bool operator ==(MyTime t1, MyTime t2)
    {
        return t1.Hour == t2.Hour && t1.Minute == t2.Minute && t1.Second == t2.Second;
    }

    public static bool operator !=(MyTime t1, MyTime t2)
    {
        return t1.Hour != t2.Hour || t1.Minute != t2.Minute || t1.Second != t2.Second;
    }
}