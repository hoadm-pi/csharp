﻿// See https://aka.ms/new-console-template for more information

using PT;

MyTime t1 = new MyTime(11, 59, 0);
MyTime t2 = new MyTime(21, 0, 0);

Console.WriteLine(t1);
t2.Print();

Console.WriteLine(t1 == t2);

t1.AddSecond(60);
t1.AddMinute(60);
t1.AddHour(8);
Console.WriteLine(t1 == t2);