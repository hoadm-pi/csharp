namespace E2;

public abstract class Book
{
    private string _title;
    private string _author;
    private string _publisher;
    private double _price;

    protected Book()
    {
        _title = "";
        _author = "";
        _publisher = "";
        _price = 0.0;
    }

    protected Book(string title, string author, string publisher, double price)
    {
        _title = title;
        _author = author;
        _publisher = publisher;
        _price = 0.0;
    }
    
    public string Title { get; set; }
    public string Author { get; set; }
    public string Publisher { get; set; }

    public double Price
    {
        get => _price;
        set => _price = value > 0 ? value : 0;
    }
    public abstract void Input();
    public abstract void Print();
    public abstract double GetPrice(int qty);
}