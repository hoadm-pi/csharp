namespace E2;

public class PBook : Book
{
    public static double TAX = 0.1;
    private int _page;

    public PBook()
        :base()
    {
        _page = 0;
    }

    public PBook(string title, string author, string publisher, double price, int page)
        :base(title, author, publisher, price)
    {
        _page = page;
    }

    public int Page
    {
        get => _page;
        set => _page = value > 0 ? value : 0;
    }
    
    public override void Input()
    {
        Console.Write("Title: ");
        Title = Console.ReadLine();
        
        Console.Write("Author: ");
        Author = Console.ReadLine();
        
        Console.Write("Publisher: ");
        Publisher = Console.ReadLine();
        
        Console.Write("Page: ");
        Page = int.Parse(Console.ReadLine());
        
        Console.Write("Price: ");
        Price = double.Parse(Console.ReadLine());
    }

    public override void Print()
    {
        Console.WriteLine($"Title: {Title}");
        Console.WriteLine($"Author: {Author}");
        Console.WriteLine($"Publisher: {Publisher}");
        Console.WriteLine($"Page: {Page}");
        Console.WriteLine($"Price: {Price}");
    }

    public override double GetPrice(int qty)
    {
        return Price * qty * (1 + PBook.TAX);
    }

    public override string ToString()
    {
        return $"{Title} - {Author} ({Page})";
    }
}