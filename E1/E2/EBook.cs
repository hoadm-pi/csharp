namespace E2;

public class EBook : Book
{
    public static double TAX = 0.05;
    private string _format;

    public EBook()
        :base()
    {
        _format = "";
    }

    public EBook(string title, string author, string publisher, double price, string format)
        :base(title, author, publisher, price)
    {
        _format = format;
    }
    
    public string Format { get; set; }
    
    public override void Input()
    {
        Console.Write("Title: ");
        Title = Console.ReadLine();
        
        Console.Write("Author: ");
        Author = Console.ReadLine();
        
        Console.Write("Publisher: ");
        Publisher = Console.ReadLine();
        
        Console.Write("Format: ");
        Format = Console.ReadLine();
        
        Console.Write("Price: ");
        Price = double.Parse(Console.ReadLine());
    }

    public override void Print()
    {
        Console.WriteLine($"Title: {Title}");
        Console.WriteLine($"Author: {Author}");
        Console.WriteLine($"Publisher: {Publisher}");
        Console.WriteLine($"Format: {Format}");
        Console.WriteLine($"Price: {Price}");
    }

    public override double GetPrice(int qty)
    {
        return Price * qty * (1 + EBook.TAX);
    }

    public override string ToString()
    {
        return $"{Title} - {Author} ({Format})";
    }
}