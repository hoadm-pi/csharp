namespace E3;

public class Car: Vehicle
{
    public static double TAX = 0.07;
    private double _price;
    private int _days;

    public Car()
        :base()
    {
    }

    public Car(string model, string color, int year, double price)
        :base(model, color, year)
    {
        _price = price;
    }

    public void Rent(int days)
    {
        _days = days;
    }

    public override double GetPrice()
    {
        return _price * _days * (1 + Car.TAX);
    }

    public override string ToString()
    {
        return base.ToString() + $" - {_price}";
    }
}