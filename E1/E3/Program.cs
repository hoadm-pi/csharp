﻿// See https://aka.ms/new-console-template for more information


using E3;

Car c = new Car("BMW", "White", 2022, 1_500_000);
c.Rent(3);
Console.WriteLine(c);
Console.WriteLine(c.GetPrice());


Truck t = new Truck("Thaco", "Blue", 2022);
t.Rent(750, 10);
Console.WriteLine(t);
Console.WriteLine(t.GetPrice());

