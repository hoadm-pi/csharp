namespace E3;

public class Truck:Vehicle
{
    public static double TAX = 0.05;
    private static double PRICE = 15000;
    private int _wegiht;
    private int _distance;

    public Truck()
        :base()
    {
        
    }

    public Truck(string model, string color, int year)
        :base(model, color, year)
    {
    }

    public void Rent(int weight, int distance)
    {
        _wegiht = weight;
        _distance = distance;
    }

    public override double GetPrice()
    {
        return (Truck.PRICE * _wegiht * _distance) * (1 + Truck.TAX);
    }

    public override string ToString()
    {
        return base.ToString() + $" - {Truck.PRICE}";
    }
}