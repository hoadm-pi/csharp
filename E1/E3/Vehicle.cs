namespace E3;

public class Vehicle
{
    private string _model;
    private string _color;
    private int _year;

    public string Model
    {
        get => _model; 
        set => _model = value;
    }

    public string Color
    {
        get => _color; 
        set => _color = value;
    }
    
    public int Year
    {
        get => _year;
        set => _year = value > 1990 ? value : 1990;
    }
    
    public Vehicle()
    {
        _model = "";
        _color = "";
        _year = 1990;
    }

    public Vehicle(string model, string color, int year)
    {
        _model = model;
        _color = color;
        _year = year;
    }

    public virtual double GetPrice()
    {
        return 0;
    }
    
    public override string ToString()
    {
        return $"{Model} - {Color} - {Year}";
    }
}