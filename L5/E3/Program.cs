﻿using System;

namespace E3
{
    class Program
    {
        /*
         * Bài tập 3 (slide bài giảng 5 - trang 32) 
         * Class BankAccount
         * 
         * Hãy xác định các thuộc tính và phương thức cho lớp BankAccount mô 
         * tả các đối tượng là các tài khoản ngân hàng
        */

        static void Main(string[] args)
        {
            BankAccount acc = new BankAccount();
            acc.Input();

            Console.WriteLine("------------");
            Console.WriteLine($"{acc}");

            Console.WriteLine("------------");
            Console.Write("Deposit: ");
            double depositAmount = double.Parse(Console.ReadLine());
            acc.Deposit(depositAmount);
            Console.WriteLine($"{acc}");
            Console.WriteLine("------------");


            Console.Write("Withdraw: ");
            double withdrawAmount = double.Parse(Console.ReadLine());
            if (acc.Withdraw(withdrawAmount))
            {
                Console.WriteLine("Withdraw successfully");
                Console.WriteLine($"{acc}");
            } else
            {
                Console.WriteLine("Balance is not enough!!!");
                Console.WriteLine($"{acc}");
            }
            
            Console.WriteLine("------------");
        }
    }
}
