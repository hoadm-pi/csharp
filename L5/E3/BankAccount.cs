﻿using System;
namespace E3
{
    public class BankAccount
    {
        public string accountNumber;
        public string owner;
        public double balance;

        public BankAccount()
        {
        }

        public void Input()
        {
            Console.Write("accountNumber: ");
            accountNumber = Console.ReadLine();

            Console.Write("owner: ");
            owner = Console.ReadLine();

            Console.Write("balance: ");
            balance = double.Parse(Console.ReadLine()) ;
        }

        public override string ToString()
        {
            return $"{accountNumber} - {owner} - {balance:C2}";
        }

        public void Deposit(double amount)
        {
            balance += amount;
        }

        public bool Withdraw(double amount)
        {
            if (amount > balance)
            {
                // số dư không đủ
                return false;
            }

            balance -= amount;
            return true;
        }

    }
}
