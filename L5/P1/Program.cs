﻿using System;

namespace P1
{
    class Program
    {
        static void Main(string[] args)
        {
            /* TH1: Sử dụng constructor rỗng, dùng hàm Input nhập thông tin */
            // Student std = new Student();
            // std.Input();

            /* TH2: Sử dụng constructor khởi tạo đối tượng */
            Student std = new Student("11DH110005", "Minh Hoa", 1993, true, "TH1102");


            /* Sử dụng tất cả các hàm đã định nghĩa */
            // In dữ liệu khi mới khởi tạo
            Console.WriteLine("----------------");
            std.PrintInfo();
            Console.WriteLine("----------------");

            // Sử dụng các phương thức Get để in dữ liệu

            Console.WriteLine($"Student ID: { std.GetStudentId() }");
            Console.WriteLine($"Name: { std.GetName() }");
            Console.WriteLine($"Birth Year: { std.GetBirthYear() } ({ std.GetAge() })");
            if (std.GetGender())
            {
                Console.WriteLine($"Gender: Male");
            }
            else
            {
                Console.WriteLine($"Gender: Female");
            }

            Console.WriteLine($"Class: {std.GetStdClass()}");
            Console.WriteLine("----------------");

            // Sử dụng các phương thức Set để cập nhật lại dữ liệu
            std.SetStudentId("MIT19203");
            std.SetName("Hoa Dinh");
            std.SetBirthYear(1993);
            std.SetGender(true);
            std.SetStdClass("MIT192");

            // In dữ liếu sau khi đã cập nhật

            std.PrintInfo();
            Console.WriteLine("----------------");
        }
    }
}
