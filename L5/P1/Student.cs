﻿using System;
namespace P1
{
    public class Student
    {
        private string studentId;
        private string name;
        private int birthYear;
        private bool gender;
        private string stdClass;

        public Student()
        {
        }

        public Student(string studentId, string name, int birthYear, bool gender, string stdClass)
        {
            DateTime now = DateTime.Today;

            this.studentId = studentId;
            this.name = name;
            if (birthYear >= 1900 && birthYear <= now.Year)
            {
                this.birthYear = birthYear;
            } else
            {
                Console.WriteLine("birthYear is not valid!");
                this.birthYear = 1900;
            }
            
            this.gender = gender;
            this.stdClass = stdClass;
        }

        public string GetStudentId()
        {
            return studentId;
        }

        public void SetStudentId(string newId)
        {
            studentId = newId;
        }

        public string GetName()
        {
            return name;
        }

        public void SetName(string newName)
        {
            name = newName;
        }


        public int GetBirthYear()
        {
            return birthYear;
        }

        public void SetBirthYear(int newbirthYear)  
        {
            birthYear = newbirthYear;
        }

        public bool GetGender()
        {
            return gender;
        }

        public void SetGender(bool newGender)
        {
            gender = newGender;
        }

        public string GetStdClass()
        {
            return stdClass;
        }

        public void SetStdClass(string newStdClass)
        {
            stdClass = newStdClass;
        }

        public void Input()
        {
            Console.Write("studentId: ");
            studentId = Console.ReadLine();

            Console.Write("name: ");
            name = Console.ReadLine();

            DateTime now = DateTime.Today;
            int inputBirthYear = 0;
            do
            {
                Console.Write("birthYear: ");
                inputBirthYear = int.Parse(Console.ReadLine());

            } while (inputBirthYear <= 1900 || inputBirthYear > now.Year);

            birthYear = inputBirthYear;


            Console.Write("gender (M/F): ");
            string genderInput = Console.ReadLine();
            gender = (genderInput == "M");

            Console.Write("stdClass: ");
            stdClass = Console.ReadLine();
        }

        public int GetAge()
        {
            DateTime now = DateTime.Today;
            return now.Year - birthYear;
        }

        public void PrintInfo()
        {
            Console.WriteLine($"Student ID: { studentId }");
            Console.WriteLine($"Name: { name }");
            Console.WriteLine($"Birth Year: { birthYear } ({this.GetAge()})");
            if (gender)
            {
                Console.WriteLine($"Gender: Male");
            } else
            {
                Console.WriteLine($"Gender: Female");
            }
            
            Console.WriteLine($"Class: {stdClass}");
        }
    }
}
