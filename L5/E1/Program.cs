﻿using System;

namespace E1
{
    class Program
    {
        /*
         * Bài tập 1 (slide bài giảng 5 - trang 30) 
         * Class Fraction
         * 
         * Hãy xác định các thuộc tính và phương thức cho lớp Fraction mô tả 
         * các đối tượng là các phân số
         */

        static void Main(string[] args)
        {
            Fraction f1 = new Fraction();
            Fraction f2 = new Fraction();

            f1.Input();
            f2.Input();
            Console.WriteLine("---------");

            Console.WriteLine("{0} = {1}", f1, f1.Decimal());
            Console.WriteLine("{0} = {1}", f2, f2.Decimal());
            Console.WriteLine("-----------");

            Fraction sum = f1.Add(f2);
            Console.WriteLine("{0} + {1} = {2}", f1, f2, sum);
            Console.WriteLine("-----------");

            Fraction different = f1.Subtract(f2);
            Console.WriteLine("{0} - {1} = {2}", f1, f2, different);
            Console.WriteLine("-----------");

            Fraction product = f1.Multiply(f2);
            Console.WriteLine("{0} * {1} = {2}", f1, f2, product);
            Console.WriteLine("-----------");

            Fraction quotient = f1.Divide(f2);
            Console.WriteLine("{0} : {1} = {2}", f1, f2, quotient);
            Console.WriteLine("-----------");

        }
    }
}
