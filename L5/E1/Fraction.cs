﻿using System;
namespace E1
{
    public class Fraction
    {
        public int numerator;
        public int denominator;

        public Fraction()
        {
        }

        public Fraction(int numerator, int demoninator)
        {
            this.numerator = numerator;
            this.denominator = demoninator;
        }

        public Fraction(int numerator)
        {
            this.numerator = numerator;
            this.denominator = 1;
        }

        public void Input()
        {
            Console.Write("numerator: ");
            numerator = int.Parse(Console.ReadLine());

            Console.Write("denominator: ");
            denominator = int.Parse(Console.ReadLine());
        }

        public void Simplify()
        {
            int a = Math.Abs(numerator);
            int b = Math.Abs(denominator);
            int remain;

            while (b!=0)
            {
                remain = a % b;
                a = b;
                b = remain;
            }

            int gcd = a;

            numerator /= gcd;
            denominator /= gcd;
        }

        public override string ToString()
        {
            return $"{numerator}/{denominator}";
        }

        public double Decimal()
        {
            return (double)numerator / denominator;
        }

        public Fraction Add(Fraction f)
        {
            Fraction sum = new Fraction();

            if (denominator == f.denominator)
            {
                // mẫu bằng nhau -> mẫu giữ nguyên, cộng tử số
                sum.denominator = denominator;
                sum.numerator = numerator + f.numerator;
            } else
            {
                // khác mẫu
                // 1. quy đồng mẫu
                int _denominator = denominator;

                numerator *= f.denominator;
                denominator *= f.denominator;

                f.numerator *= _denominator;
                f.denominator *= _denominator;

                // 2. cộng
                sum.denominator = denominator;
                sum.numerator = numerator + f.numerator;
            }

            sum.Simplify();

            return sum;
        }

        public Fraction Subtract(Fraction f)
        {
            Fraction different = new Fraction();

            if (denominator == f.denominator)
            {
                // mẫu bằng nhau -> mẫu giữ nguyên, trừ tử số
                different.denominator = denominator;
                different.numerator = numerator - f.numerator;
            }
            else
            {
                // khác mẫu
                // 1. quy đồng mẫu
                int _denominator = denominator;

                numerator *= f.denominator;
                denominator *= f.denominator;

                f.numerator *= _denominator;
                f.denominator *= _denominator;

                // 2. trừ
                different.denominator = denominator;
                different.numerator = numerator - f.numerator;
            }

            different.Simplify();

            return different;
        }

        public Fraction Multiply(Fraction f)
        {
            Fraction product = new Fraction();
            product.numerator = numerator * f.numerator;
            product.denominator = denominator * f.denominator;
            product.Simplify();

            return product;
        }

        public Fraction Divide(Fraction f)
        {
            Fraction quotient = new Fraction();
            quotient.numerator = numerator * f.denominator;
            quotient.denominator = denominator * f.numerator;
            quotient.Simplify();

            return quotient;
        }
    }
}
