﻿using System;

namespace P4
{
    class Program
    {
        static void Main(string[] args)
        {
            Date dt = new Date(13, 3, 2022);
            Console.WriteLine(dt);

            // Sử dụng các hàm Get - Set
            dt.SetDay(1);
            dt.SetMonth(5);
            dt.SetYear(2023);

            Console.WriteLine("{0:00}/{1:00}/{2}", dt.GetDay(), dt.GetMonth(), dt.GetYear());
        }
    }
}
