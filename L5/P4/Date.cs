﻿using System;
namespace P4
{
    public class Date
    {
        private int day;
        private int month;
        private int year;

        public Date(int day, int month, int year)
        {
            this.day = day;
            this.month = month;
            this.year = year;
        }

        public int GetDay()
        {
            return day;
        }

        public int GetMonth()
        {
            return month;
        }

        public int GetYear()
        {
            return year;
        }

        public void SetDay(int day)
        {
            this.day = day;
        }

        public void SetMonth(int month)
        {
            this.month = month;
        }

        public void SetYear(int year)
        {
            this.year = year;
        }

        public void SetDay(int day, int month, int year)
        {
            this.day = day;
            this.month = month;
            this.year = year;
        }

        public override string ToString()
        {
            return $"{day:00}/{month:00}/{year}";
        }
    }
}
