﻿using System;
namespace P5
{
    public class Author
    {
        private string name;
        private string email;
        private char gender;

        public Author(string name, string email, char gender)
        {
            this.name = name;
            this.email = email;
            this.gender = gender;
        }

        public string GetName()
        {
            return name;
        }

        public string GetEmail()
        {
            return email;
        }

        public char GetGender()
        {
            return gender;
        }

        public void SetEmail(string email)
        {
            this.email = email;
        }

        public override string ToString()
        {
            return $"Author[name={name}, email={email}, gender={gender}]";
        }
    }
}
