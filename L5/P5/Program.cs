﻿using System;

namespace P5
{
    class Program
    {
        static void Main(string[] args)
        {
            // Author

            Author nna = new Author("Nguyen Nhat Anh", "nguyennhatanh@gmail.com", 'm');
            Console.WriteLine(nna);
            Console.WriteLine("-------------");


            Book nktc = new Book("Ngoi khoc tren cay", nna, 93500);
            nktc.SetQty(10);

            Console.WriteLine(nktc);
            Console.WriteLine("-------------");
        }
    }
}
