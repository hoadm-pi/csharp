﻿using System;
namespace P5
{
    public class Book
    {
        private string name;
        private Author author;
        private double price;
        private int qty = 0;

        public Book(string name, Author author, double price)
        {
            this.name = name;
            this.author = author;
            this.price = price;
        }

        public Book(string name, Author author, double price, int qty)
        {
            this.name = name;
            this.author = author;
            this.price = price;
            this.qty = qty;
        }

        public string GetName()
        {
            return name;
        }

        public Author GetAuthor()
        {
            return author;
        }

        public double GetPrice()
        {
            return price;
        }

        public int GetQty()
        {
            return qty;
        }

        public void SetQty(int qty)
        {
            this.qty = qty;
        }

        public void SetPrice(double price)
        {
            this.price = price;
        }

        public override string ToString()
        {
            return $"Book[name={name}, {author}, price={price}, qty={qty}]";
        }
    }
}
