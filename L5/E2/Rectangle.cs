﻿using System;
namespace E2
{
    public class Rectangle
    {
        public double width;
        public double height;

        public Rectangle()
        {
        }

        public void Input()
        {
            Console.Write("Width: ");
            width = int.Parse(Console.ReadLine());

            Console.Write("Height: ");
            height = int.Parse(Console.ReadLine());
        }

        public override string ToString()
        {
            return $"Rectangle({width}, {height})";
        }

        public double GetPerimeter()
        {
            return (width + height) * 2;
        }

        public double GetArea()
        {
            return width * height;
        }

        public bool IsSameArea(Rectangle rect)
        {
            return GetArea() == rect.GetArea();
        }
    }
}
