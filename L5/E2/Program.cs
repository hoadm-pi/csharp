﻿using System;

namespace E2
{
    class Program
    {
        /*
         * Bài tập 2 (slide bài giảng 5 - trang 31) 
         * Class Rectangle
         * 
         * Hãy xác định các thuộc tính và phương thức cho lớp Rectangle 
         * mô tả các đối tượng là các hình chữ nhật
         */


        static void Main(string[] args)
        {
            Rectangle rect1 = new Rectangle();
            Rectangle rect2 = new Rectangle();

            rect1.Input();
            rect2.Input();
            Console.WriteLine("---------");

            Console.WriteLine("{0}", rect1);
            Console.WriteLine("Perimeter: {0}", rect1.GetPerimeter());
            Console.WriteLine("Area: {0}", rect1.GetArea());
            Console.WriteLine("---------");

            Console.WriteLine("{0}", rect2);
            Console.WriteLine("Perimeter: {0}", rect2.GetPerimeter());
            Console.WriteLine("Area: {0}", rect2.GetArea());
            Console.WriteLine("---------");

            if (rect1.IsSameArea(rect2))
            {
                Console.WriteLine("Area rect1 == Area rect2");
            }
            else
            {
                Console.WriteLine("Area rect1 != Area rect2");
            }
        }
    }
}
