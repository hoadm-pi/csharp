﻿using System;

namespace E7
{
    class Program
    {
        /*
        * Bài tập 7 (slide bài giảng 2 - trang 32) 
        * Cài đặt lại thuật toán Tìm kiếm nhị phân bằng cách đệ quy
        * 
        * 1, 4, 7, 9, 16, 56, 70
        */

        public static int[] InputArray()
        {
            Console.Write("Please enter the number of elements: ");
            int n = int.Parse(Console.ReadLine());
            int[] arr = new int[n];

            for (int i = 0; i < n; i++)
            {
                Console.Write("Element {0}: ", i + 1);
                arr[i] = int.Parse(Console.ReadLine());
            }

            return arr;
        }

        public static void Show(int[] arr)
        {
            Console.Write("Arr: ");
            foreach (int e in arr)
            {
                Console.Write("{0} ", e);
            }
            Console.WriteLine();
            Console.WriteLine("-----------------");
        }


        static int BinarySearch(int[] arr, int left, int right, int element)
        {
            if (right < left)
            {
                return -1;
            }
            else
            {
                int middle = left + (right - left) / 2;
                if (arr[middle] == element)
                {
                    return middle;
                } else if (arr[middle] > element)
                {
                    return BinarySearch(arr, left, middle - 1, element);
                } else
                {
                    return BinarySearch(arr, middle + 1, right, element);
                }

            }    
        }
        static void Main(string[] args)
        {
            int[] arr = InputArray();
            Show(arr);

            Console.Write("Find: ");
            int x = int.Parse(Console.ReadLine());

            int idx = BinarySearch(arr, 0, arr.Length, x);
            if (idx == -1)
            {
                Console.WriteLine("Not found!!!");
            } else
            {
                Console.WriteLine($"Found {x} at position: {idx}!");
            }    
        }
    }
}
