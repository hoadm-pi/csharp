﻿using System;

namespace E1
{
    class Program
    {
        /*
         * Bài tập 1 (slide bài giảng 2 - trang 11) 
         * Hãy xác định công thức đệ quy để tính a^n, 
         * với a là một số thực và n là số nguyên không âm
         */

        //static double Pow(double a, int n)
        //{
        //    if (n == 0)
        //    {
        //        return 1;
        //    } else
        //    {
        //        return a * Pow(a, n - 1);
        //    }    
        //}

        static double Pow(double a, int n)
        {
            if (n == 0)
            {
                return 1;
            } else
            {
                if (n % 2 == 0)
                {
                    return Pow(a, n / 2) * Pow(a, n / 2);
                } else
                {
                    return a * Pow(a, (n - 1) / 2) * Pow(a, (n - 1) / 2);
                }    
            }
        }

        static void Main(string[] args)
        {
            Console.Write("Input m, n: ");
            string[] tokens = Console.ReadLine().Split();
            double a = double.Parse(tokens[0]);
            int n = int.Parse(tokens[1]);


            Console.WriteLine("{0}^{1} = {2}", a, n, Pow(a, n));
        }
    }
}
