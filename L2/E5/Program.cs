﻿using System;

namespace E5
{
    class Program
    {
        /*
        * Bài tập 5 (slide bài giảng 2 - trang 27) 
        * Viết hàm đệ quy tính 𝒂^𝒏  theo công thức trên có cải tiến 
        * bằng cách sử dụng biến phụ
        */

        static double Pow(double a, int n)
        {
            if (n == 0)
            {
                return 1;
            }
            else
            {
                if (n % 2 == 0)
                {
                    double t = Pow(a, n / 2);
                    return t * t;
                }
                else
                {
                    double t = Pow(a, (n - 1) / 2);
                    return a * t * t;
                }
            }
        }

        static void Main(string[] args)
        {
            Console.Write("Input m, n: ");
            string[] tokens = Console.ReadLine().Split();
            double a = double.Parse(tokens[0]);
            int n = int.Parse(tokens[1]);


            Console.WriteLine("{0}^{1} = {2}", a, n, Pow(a, n));
        }
    }
}
