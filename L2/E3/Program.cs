﻿using System;

namespace E3
{
    class Program
    {
        /*
        * Bài tập 3 (slide bài giảng 2 - trang 13) 
        * Hãy xác định công thức đệ quy để tìm ước số chung lớn nhất 
        * của 2 số nguyên a, b theo giải thuật Euclid:
        */

        //static int GCD(int a, int b)
        //{
        //    if (a == b)
        //    {
        //        return a;
        //    }
        //    else if (a > b)
        //    {
        //        return GCD(a - b, b);
        //    } else
        //    {
        //        return GCD(a, b - a);
        //    }
        //}

        static int GCD(int a, int b)
        {
            if (b == 0)
            {
                return a;
            }
            else
            {
                return GCD(b, a % b);
            }    
        }

        static void Main(string[] args)
        {
            Console.Write("Input b, c: ");
            string[] tokens = Console.ReadLine().Split();
            int a = int.Parse(tokens[0]);
            int b = int.Parse(tokens[1]);

            Console.Write("GCD({0}, {1}) = {2}", a, b, GCD(a, b));
        }
    }
}
