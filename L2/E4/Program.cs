﻿using System;

namespace E4
{
    class Program
    {
        /*
        * Bài tập 4 (slide bài giảng 2 - trang 17) 
        * Hiện thực hàm đệ quy tính n giai thừa
        */

        static int Factorial(int n)
        {
            if (n == 0)
            {
                return 1;
            }
            else
            {
                return n * Factorial(n - 1);
            }    
        }

        static void Main(string[] args)
        {
            Console.Write("Input n: ");
            string[] tokens = Console.ReadLine().Split();
            int n = int.Parse(tokens[0]);

            Console.Write("{0}! = {1}", n, Factorial(n));
        }
    }
}
