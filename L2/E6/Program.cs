﻿using System;

namespace E6
{
    class Program
    {
        /*
        * Bài tập 6 (slide bài giảng 2 - trang 31) 
        * Viết hàm đệ quy chuyển đổi một số nguyên dương n từ hệ thập phân (DEC)
        * sang hệ nhị phân (BIN)
        * 
        * input: 20
        * output: 10100
        */

        static int dec2bin(int n)
        {
            if (n <= 0)
            {
                return 0;
            }
            else
            {
                return 10 * dec2bin(n / 2) + (n % 2);
            }    
        }

        static void Main(string[] args)
        {
            Console.Write("Input n: ");
            string[] tokens = Console.ReadLine().Split();
            int n = int.Parse(tokens[0]);


            Console.WriteLine(dec2bin(n));
        }
    }
}
