﻿using System;

namespace E2
{
    class Program
    {
        /*
        * Bài tập 2 (slide bài giảng 2 - trang 12) 
        * Hãy xác định công thức đệ quy để tìm số Fibonacci thứ n, 
        * với n là một số nguyên dương và dãy số Fibonacci có dạng:
        * 
        * 1 1 2 3 5 8 13 21 ....
        */

        static int Fibonacci(int n)
        {
            if (n <= 2)
            {
                return 1;
            }
            else
            {
                return Fibonacci(n - 1) + Fibonacci(n - 2);
            }    
        }

        static void Main(string[] args)
        {
            Console.Write("Input n: ");
            string[] tokens = Console.ReadLine().Split();
            int n = int.Parse(tokens[0]);

            Console.Write("Fibonacci({0}) = {1}", n, Fibonacci(n));

        }
    }
}
