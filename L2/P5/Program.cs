﻿using System;

namespace P5
{
    class Program
    {
        /*
         * Bài tập 5
         * Bài toán đôi thỏ -> Bài toán Fibonacci (E2)
         * Đặt tên function lại để phù hợp với đề bài
         */

        static int Rabbit(int n)
        {
            if (n <= 2)
            {
                return 1;
            }
            else
            {
                return Rabbit(n - 1) + Rabbit(n - 2);
            }
        }

        static void Main(string[] args)
        {
            Console.Write("Month: ");
            string[] tokens = Console.ReadLine().Split();
            int n = int.Parse(tokens[0]);

            Console.Write("Month {0} has {1} rabbit pair(s)!", n, Rabbit(n));
        }
    }
}
