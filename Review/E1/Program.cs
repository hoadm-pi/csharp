﻿// See https://aka.ms/new-console-template for more information

using E1;

Rectangle rect = new Rectangle(2, 8);
Console.WriteLine(rect);
Console.WriteLine($"Perimeter: {rect.GetPerimeter()}");
Console.WriteLine($"Area: {rect.GetArea()}");
Console.WriteLine("---------------");

Square sqr = new Square(4);
Console.WriteLine(sqr);
Console.WriteLine($"Perimeter: {sqr.GetPerimeter()}");
Console.WriteLine($"Area: {sqr.GetArea()}");
Console.WriteLine("---------------");

if (rect == sqr)
{
    Console.WriteLine("Equal");
}
else
{
    Console.WriteLine("Not Equal");
}

Console.WriteLine("---------------");
Cuboid cub = new Cuboid(6, 3, 3);
Console.WriteLine(cub);
Console.WriteLine($"SurfaceArea: {cub.GetSurfaceArea()}");
Console.WriteLine($"Volume: {cub.GetVolume()}");
Console.WriteLine("---------------");