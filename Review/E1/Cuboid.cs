namespace E1;

public class Cuboid:Rectangle
{
    private double height;

    public double Height
    {
        get
        {
            return this.height;
        }

        set
        {
            this.height = value;
        }
    }

    public Cuboid()
        :base()
    {
        
    }

    public Cuboid(double width, double length, double height)
        : base(width, length)
    {
        this.height = height;
    }
    
    public double GetVolume()
    {
        // Diện tích đáy * chiều cao
        return base.GetArea() * this.Height;
    }

    public double GetSurfaceArea()
    {
        // 2 * diện tích đáy + chu vi đáy * chiều cao
        return 2 * base.GetArea() + base.GetPerimeter() * this.Height;
    }

    public override string ToString()
    {
        return $"Cuboid({base.Width}-{base.Length}-{this.Length})";
    }
}