namespace E1;

public class Square:Rectangle
{
    public Square()
        : base()
    {

    }
    
    public Square(double side)
        :base(side, side)
    {
        
    }

    public override string ToString()
    {
        return $"Square({base.Width})"; // hoặc base.Length
    }
    
    
}