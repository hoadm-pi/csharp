namespace E1;

public class Rectangle
{
    // fields
    protected double width;
    protected double length;

    // properties
    public double Width
    {
        get { return this.width; }

        set { this.width = value; }
    }

    public double Length
    {
        get { return this.length; }

        set { this.length = value; }
    }

    // constructors
    public Rectangle()
    {
    }

    public Rectangle(double width, double lenght)
    {
        this.width = width;
        this.length = lenght;
    }

    // methods
    public double GetPerimeter()
    {
        return (this.Width * this.Length) * 2;
    }

    public double GetArea()
    {
        return this.Width * this.Length;
    }

    public override string ToString()
    {
        return $"Rectangle({this.Width}-{this.Length})";
    }

    public static bool operator ==(Rectangle r1, Rectangle r2)
    {
        return r1.GetArea() == r2.GetArea();
    }

    public static bool operator !=(Rectangle r1, Rectangle r2)
    {
        return r1.GetArea() != r2.GetArea();
    }

}