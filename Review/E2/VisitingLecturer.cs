namespace E2;

public class VisitingLecturer : Lecturer
{
    private int hours;

    public VisitingLecturer()
        :base()
    {
        
    }

    public VisitingLecturer(string id, string name, bool gender, string faculty)
        : base(id, name, gender, faculty)
    {
        
    }

    public void SetHours(int hours)
    {
        this.hours = hours;
    }

    public override double GetSalary()
    {
        return 200_000 * this.hours;
    }

    public override string ToString()
    {
        string genStr = this.Gender ? "Male" : "Female";
        return $"{this.Id} - {this.Name} - {genStr} - {this.hours}";
    }
}