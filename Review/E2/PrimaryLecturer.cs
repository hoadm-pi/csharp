namespace E2;

public class PrimaryLecturer : Lecturer
{
    private static double baseRate = 10_000_000;
    private double payRate;
    public PrimaryLecturer()
        :base()
    {
        
    }

    public PrimaryLecturer(string id, string name, bool gender, string faculty, double payRate)
        :base(id, name, gender, faculty)
    {
        this.payRate = payRate;
    }

    public void Promotion()
    {
        this.payRate *= 1.25;
    }

    public override double GetSalary()
    {
        return this.payRate * PrimaryLecturer.baseRate;
    }

    public override string ToString()
    {
        string genStr = this.Gender ? "Male" : "Female";
        return $"{this.Id} - {this.Name} - {genStr} - {this.Faculty} - {this.payRate}";
    }
}