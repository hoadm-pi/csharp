namespace E2;

public abstract class Lecturer
{
    private string id;
    private string name;
    private bool gender;
    private string faculty;

    public Lecturer()
    {
        
    }

    protected Lecturer(string id, string name, bool gender, string faculty)
    {
        this.id = id;
        this.name = name;
        this.gender = gender;
        this.faculty = faculty;
    }

    public string Id
    {
        get
        {
            return this.id;
        }

        set
        {
            this.id = value;
        }
    }

    public string Name
    {
        get
        {
            return this.name;
        }
        set
        {
            this.name = value;
        }
    }

    public bool Gender
    {
        get
        {
            return this.gender;
        }
        set
        {
            this.gender = value;
        }
    }

    public string Faculty
    {
        get
        {
            return this.faculty;
        }
        set
        {
            this.faculty = value;
        }
    }

    public abstract double GetSalary();
}