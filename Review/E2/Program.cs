﻿// See https://aka.ms/new-console-template for more information

using E2;

VisitingLecturer vLec = new VisitingLecturer("04092", "Van Minh", true, "CNTT");
vLec.SetHours(80);
Console.WriteLine(vLec);
Console.WriteLine($"Salary: {vLec.GetSalary()}");

Console.WriteLine("---------------");

PrimaryLecturer pLec = new PrimaryLecturer("02024", "Thanh Thanh", false, "CNTT", 2.5);
Console.WriteLine(pLec);
Console.WriteLine($"Salary: {pLec.GetSalary()}");
pLec.Promotion();
Console.WriteLine($"Salary: {pLec.GetSalary()}");
