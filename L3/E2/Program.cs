﻿using System;

namespace E2
{
    class Program
    {
        /*
         * Bài tập 2 (slide bài giảng 3 - trang 14) 
         * 
         * Trò chơi Tháp Hà Nội (Tower of Hanoi) Có 3 chiếc cột đặt tên là 
         * A, B và C. Ban đầu, đặt n cái đĩa có kích thước khác nhau ở cột A 
         * theo thứ tự kích thước các đĩa nhỏ dần từ đáy đến đỉnh cột 
         * (đĩa nhỏ nhất nằm trên cùng – xem hình bên)
         * 
         * Input: một dòng duy nhất chứa số nguyên n – số đĩa ban đầu ở cột A
         * 
         * Output: Gồm nhiều dòng, mô tả các bước di chuyển đĩa, theo định dạng:
         * 
         * Chuyển 1 đĩa từ cột X -> Y
         */

        static void TowerOfHanoi(int n, char source, char dest, char aux)
        {
            if (n == 1)
            {
                Console.WriteLine($"Di chuyển 1 đĩa từ cột {source} -> {dest}");
            } else
            {
                TowerOfHanoi(n - 1, source, aux, dest);
                TowerOfHanoi(1, source, dest, aux);
                TowerOfHanoi(n - 1, aux, dest, source);
            }
        }

        static void Main(string[] args)
        {
            Console.Write("Input n: ");
            int n = int.Parse(Console.ReadLine());

            TowerOfHanoi(n, 'A', 'C', 'B');
        }
    }
}
