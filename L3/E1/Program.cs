﻿using System;

namespace E1
{
    class Program
    {
        /*
         * Bài tập 1 (slide bài giảng 3 - trang 11) 
         * 
         * Cho dãy số nguyên a gồm n phần tử. Tìm giá trị nhỏ nhất của dãy số
         * bằng phương pháp chia để trị.
         * 
         * Input: 
         * 5
         * 7 6 8 2 4
         * 
         * Output:
         * 2
         */
        public static int[] InputArray()
        {
            Console.Write("Please enter the number of elements: ");
            int n = int.Parse(Console.ReadLine());
            int[] arr = new int[n];

            for (int i = 0; i < n; i++)
            {
                Console.Write("Element {0}: ", i + 1);
                arr[i] = int.Parse(Console.ReadLine());
            }

            return arr;
        }


        static int Min(int[] arr, int left, int right)
        {
            if (left == right)
            {
                return arr[left];
            }

            int middle = (right + left) / 2;
            int minLeft = Min(arr, left, middle);
            int minRight = Min(arr, middle + 1, right);

            return minLeft < minRight ? minLeft : minRight;
        }

        static void Main(string[] args)
        {
            int[] arr = InputArray();
            Console.WriteLine("----------------");
            Console.WriteLine("Min element: {0}", Min(arr, 0, arr.Length - 1));
            
        }
    }
}
