﻿using System;

namespace P1
{
    class Program
    {
        /*
         * Bài tập 1 - Buổi 4 - Phương pháp chia để trị - Trang 14
         * Tìm giá trị nhỏ nhất và giá trị lớn nhất trong dãy số
         * 
         * Cách 1: Viết thành 2 hàm riêng biệt, Min và Max.
         * Cách 2: Sử dụng cấu trúc dữ liệu
         */

        public class Pair
        {
            public int min;
            public int max;
        }

        public static int[] InputArray()
        {
            Console.Write("Please enter the number of elements: ");
            int n = int.Parse(Console.ReadLine());
            int[] arr = new int[n];

            for (int i = 0; i < n; i++)
            {
                Console.Write("Element {0}: ", i + 1);
                arr[i] = int.Parse(Console.ReadLine());
            }

            return arr;
        }


        static Pair MinMax(int[] arr, int left, int right)
        {
            Pair mm = new Pair();
            Pair mmLeft = new Pair();
            Pair mmRight = new Pair();

            if (left == right)
            {
                mm.min = arr[left];
                mm.max = arr[right];

                return mm;
            }

            int middle = (right + left) / 2;
            mmLeft = MinMax(arr, left, middle);
            mmRight = MinMax(arr, middle + 1, right);

            if (mmLeft.min < mmRight.min)
            {
                mm.min = mmLeft.min;
            } else
            {
                mm.min = mmRight.min;
            }

            if (mmLeft.max > mmRight.max)
            {
                mm.max = mmLeft.max;
            }
            else
            {
                mm.max = mmRight.max;
            }

            return mm;
        }

        static void Main(string[] args)
        {
            int[] arr = InputArray();

            Pair minmax = MinMax(arr, 0, arr.Length - 1);
            Console.WriteLine("Minimum element is {0}", minmax.min);
            Console.WriteLine("Maximum element is {0}", minmax.max);
        }
    }
}
