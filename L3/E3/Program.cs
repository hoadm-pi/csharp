﻿using System;

namespace E3
{
    class Program
    {
        /*
         * Bài tập 3 (slide bài giảng 3 - trang 20) 
         * Thuật toán Sắp xếp nhanh (Quick Sort)
         */

        static int Partition(int[] arr, int left, int right)
        {
            int pivot = arr[(left + right) / 2];
            
            int i = left - 1;
            int j = right + 1;
            
            while (true)
            {
                //Tăng i cho đến khi gặp phần tử KHÔNG NHỎ HƠN pivot
                do i++; while (arr[i] < pivot);
                //Giảm j cho đến khi gặp phần tử KHÔNG LỚN HƠN pivot
                do j--; while (arr[j] > pivot);

                if (i >= j)
                {
                    return j;
                }
                    
                int tmp = arr[i];
                arr[i] = arr[j];
                arr[j] = tmp;
            }
        }

        static void QuickSort(int[] arr, int left, int right)
        {
            if (left < right)
            {
                int p = Partition(arr, left, right);

                //Sắp xếp phân đoạn (left, p)
                QuickSort(arr, left, p);

                //Sắp xếp phân đoạn (p+1, right)
                QuickSort(arr, p + 1, right);
            }
        }

        public static int[] InputArray()
        {
            Console.Write("Please enter the number of elements: ");
            int n = int.Parse(Console.ReadLine());
            int[] arr = new int[n];

            for (int i = 0; i < n; i++)
            {
                Console.Write("Element {0}: ", i + 1);
                arr[i] = int.Parse(Console.ReadLine());
            }

            return arr;
        }

        public static void Show(int[] arr)
        {
            Console.Write("Arr: ");
            foreach (int e in arr)
            {
                Console.Write("{0} ", e);
            }
            Console.WriteLine();
        }

        static void Main(string[] args)
        {
            int[] arr = InputArray();
            Show(arr);
            QuickSort(arr, 0, arr.Length - 1);
            Console.WriteLine("Sorted array");
            Show(arr);
        }
    }
}
