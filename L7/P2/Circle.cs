﻿using System;
namespace P2
{
    public class Circle:Shape
    {
        private double radius;

        public Circle(double radius)
        {
            this.radius = radius;
        }

        public Circle(double radius, string color, bool filled)
            :base(color, filled)
        {
            this.radius = radius;
        }

        public double GetRadius()
        {
            return this.radius;
        }

        public void SetRadius(double radius)
        {
            this.radius = radius;
        }

        public override double GetArea()
        {
            return this.radius * this.radius * Math.PI;
        }

        public override double GetPerimeter()
        {
            return 2 * this.radius * Math.PI;
        }

        public override string ToString()
        {
            return $"Circle\n" +
                $"Radius: {this.radius}\n" +
                $"Perimeter: {this.GetPerimeter()}\n" +
                $"Area: {this.GetArea()}";
        }

    }
}
