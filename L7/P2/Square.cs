﻿using System;
namespace P2
{
    public class Square:Rectangle
    {
        public Square()
        {
        }

        public Square(double side)
            :base(side, side)
        { }

        public Square(double side, string color, bool filled)
            :base(side, side, color, filled)
        { }

        public double GetSide()
        {
            return this.GetLength();
        }

        public void SetSide(double side)
        {
            this.SetLength(side);
            this.SetWidth(side);
        }

        // SetWidth và SetLength --> nếu khai báo nữa sẽ bị warning nhẹ, nên
        // tạm bỏ qua

        public override string ToString()
        {
            return $"Square\n" +
                $"Side: {this.GetSide()}\n" +
                $"Perimeter: {this.GetPerimeter()}\n" +
                $"Area: {this.GetArea()}";
        }
    }
}
