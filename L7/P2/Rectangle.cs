﻿using System;
namespace P2
{
    public class Rectangle:Shape
    {
        private double width;
        private double length;

        public Rectangle()
        {
        }

        public Rectangle(double width, double length)
        {
            this.width = width;
            this.length = length;
        }

        public Rectangle(double width, double length, string color, bool filled)
            :base(color, filled)
        {
            this.width = width;
            this.length = length;
        }

        public double GetWidth()
        {
            return this.width;
        }

        public void SetWidth(double width)
        {
            this.width = width;
        }

        public double GetLength()
        {
            return this.length;
        }

        public void SetLength(double length)
        {
            this.length = length;
        }

        public override double GetPerimeter()
        {
            return (this.width + this.length) * 2;
        }

        public override double GetArea()
        {
            return this.width * this.length;
        }

        public override string ToString()
        {
            return $"Rectangle\n" +
                $"Width: {this.width}\n" +
                $"Length: {this.length}\n" +
                $"Perimeter: {this.GetPerimeter()}\n" +
                $"Area: {this.GetArea()}";
        }
    }
}
