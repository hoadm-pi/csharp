﻿using System;

namespace P2
{
    class Program
    {
        static void Main(string[] args)
        {
            Circle c = new Circle(3);
            Console.WriteLine(c);

            Console.WriteLine("-----------");
            Rectangle rect = new Rectangle(3, 4);
            Console.WriteLine(rect);


            Console.WriteLine("-----------");
            Square sqr = new Square(5);
            Console.WriteLine(sqr);
        }
    }
}
