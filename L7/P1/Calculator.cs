﻿using System;
namespace P1
{
    public class Calculator
    {
        public Calculator()
        {
        }

        //public int Add(int a, int b)
        //{
        //    return a + b;
        //}

        //public double Add(double a, double b)
        //{
        //    return a + b;
        //}

        //public int Subtract(int a, int b)
        //{
        //    return a - b;
        //}

        //public double Subtract(double a, double b)
        //{
        //    return a - b;
        //}

        //public int Multiply(int a, int b)
        //{
        //    return a * b;
        //}

        //public double Multiply(double a, double b)
        //{
        //    return a * b;
        //}

        //public int Divide(int a, int b)
        //{
        //    return a / b;
        //}

        //public double Divide(double a, double b)
        //{
        //    return a / b;
        //}

        //public int Min(int a, int b)
        //{
        //    return a < b ? a : b;
        //}

        //public int Min(int a, int b, int c)
        //{
        //    int m = this.Min(a, b);
        //    return this.Min(m, c);
        //}

        //public double Min(double a, double b, double c)
        //{
        //    double m = a < b ? a : b;
        //    return m < c ? m : c;
        //}

        //public int Max(int a, int b)
        //{
        //    return a > b ? a : b;
        //}

        //public int Max(int a, int b, int c)
        //{
        //    int m = this.Max(a, b);
        //    return this.Max(m, c);
        //}

        //public double Max(double a, double b, double c)
        //{
        //    double m = a > b ? a : b;
        //    return m > c ? m : c;
        //}

        // static modifier


        public static int Add(int a, int b)
        {
            return a + b;
        }

        public static double Add(double a, double b)
        {
            return a + b;
        }

        public static int Subtract(int a, int b)
        {
            return a - b;
        }

        public static double Subtract(double a, double b)
        {
            return a - b;
        }

        public static int Multiply(int a, int b)
        {
            return a * b;
        }

        public static double Multiply(double a, double b)
        {
            return a * b;
        }

        public static int Divide(int a, int b)
        {
            return a / b;
        }

        public static double Divide(double a, double b)
        {
            return a / b;
        }

        public static int Min(int a, int b)
        {
            return a < b ? a : b;
        }

        public static int Min(int a, int b, int c)
        {
            int m = Calculator.Min(a, b);
            return Calculator.Min(m, c);
        }

        public static double Min(double a, double b, double c)
        {
            double m = a < b ? a : b;
            return m < c ? m : c;
        }

        public static int Max(int a, int b)
        {
            return a > b ? a : b;
        }

        public static int Max(int a, int b, int c)
        {
            int m = Calculator.Max(a, b);
            return Calculator.Max(m, c);
        }

        public static double Max(double a, double b, double c)
        {
            double m = a > b ? a : b;
            return m > c ? m : c;
        }

    }
}
