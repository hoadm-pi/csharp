﻿using System;
namespace E2
{
    public class MixedFraction : Fraction
    {
        public MixedFraction(int wholePart, int numerator, int denominator)
            :base(wholePart * denominator + numerator, denominator)
        {
        }

        public MixedFraction(Fraction f)
            :base(f.Numerator, f.Denomintor)
        {
        }

        public override string ToString()
        {
            int wholePart = base.Numerator / base.Denomintor;
            int numerator = base.Numerator - (wholePart * base.Denomintor);
            return string.Format("{0}[{1}/{2}]", wholePart, numerator, base.Denomintor);
        }
    }
}
