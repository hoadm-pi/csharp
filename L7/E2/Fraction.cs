﻿using System;
namespace E2
{
    public class Fraction
    {
        private int numerator;
        private int denominator;

        // Constructors
        public Fraction()
        {
            this.numerator = 0;
            this.denominator = 1;
        }

        public Fraction(int numerator, int denominator)
        {
            this.numerator = numerator;
            this.denominator = denominator;
        }

        public Fraction(int numerator)
        {
            this.numerator = numerator;
            this.denominator = 1;
        }

        // Properties
        public int Numerator
        {
            get { return this.numerator; }
            set { this.numerator = value; }
        }

        public int Denomintor
        {
            get { return this.denominator; }
            set { this.denominator = value; }
        }


        private int GCD(int a, int b)
        {
            a = Math.Abs(numerator);
            b = Math.Abs(denominator);
            int remain;

            while (b != 0)
            {
                remain = a % b;
                a = b;
                b = remain;
            }

            return a;
        }

        public void Simplify()
        {
            int gcd = this.GCD(this.numerator, this.denominator);
            this.numerator /= gcd;
            this.denominator /= gcd;
        }

        public override string ToString()
        {
            return $"{this.numerator} / {this.denominator}";
        }

        public double ToDecimal()
        {
            return (double)this.numerator / this.denominator;
        }

        //public Fraction Add(Fraction f)
        //{
        //    int numerator = this.numerator * f.denominator + this.denominator * f.numerator;
        //    int denominator = this.denominator * f.denominator;

        //    return new Fraction(numerator, denominator);
            
        //}

        //public Fraction Subtract(Fraction f)
        //{
        //    int numerator = this.numerator * f.denominator - this.denominator * f.numerator;
        //    int denominator = this.denominator * f.denominator;

        //    return new Fraction(numerator, denominator);
        //}

        //public Fraction Multifly(Fraction f)
        //{
        //    int numerator = this.numerator * f.numerator;
        //    int denominator = this.denominator * f.denominator;

        //    return new Fraction(numerator, denominator);
        //}

        //public Fraction Divide(Fraction f)
        //{
        //    int numerator = this.numerator * f.denominator;
        //    int denominator = this.denominator * f.numerator;

        //    return new Fraction(numerator, denominator);
        //}

        // Operator overloading

        // Nạp chồng toán tử một ngôi "–" để đổi dấu phân số
        public static Fraction operator -(Fraction a)
        {
            return new Fraction(-a.numerator, a.denominator);
        }

        // Nạp chồng toán tử 2 ngôi  "+, –, *, /"
        public static Fraction operator +(Fraction a, Fraction b)
        {
            int n = a.numerator * b.denominator + a.denominator * b.numerator;
            int d = a.denominator * b.denominator;
            return new Fraction(n, d);
        }

        public static Fraction operator -(Fraction a, Fraction b)
        {
            int n = a.numerator * b.denominator - a.denominator * b.numerator;
            int d = a.denominator * b.denominator;
            return new Fraction(n, d);
        }

        public static Fraction operator *(Fraction a, Fraction b)
        {
            int n = a.numerator * b.numerator;
            int d = a.denominator * b.denominator;
            return new Fraction(n, d);
        }

        public static Fraction operator /(Fraction a, Fraction b)
        {
            int n = a.numerator * b.denominator;
            int d = a.denominator * b.numerator;
            return new Fraction(n, d);
        }

        // Nạp chồng toán tử so sánh "==, !=, >, <, >=, <="

        public static bool operator ==(Fraction a, Fraction b)
        {
            return (a.numerator * b.denominator) == (a.denominator * b.numerator);
        }


        public static bool operator !=(Fraction a, Fraction b)
        {
            return (a.numerator * b.denominator) != (a.denominator * b.numerator);
        }

        public static bool operator >(Fraction a, Fraction b)
        {
            return (a.numerator * b.denominator) > (a.denominator * b.numerator);
        }

        public static bool operator <(Fraction a, Fraction b)
        {
            return (a.numerator * b.denominator) < (a.denominator * b.numerator);
        }

        public static bool operator >=(Fraction a, Fraction b)
        {
            return (a.numerator * b.denominator) >= (a.denominator * b.numerator);
        }

        public static bool operator <=(Fraction a, Fraction b)
        {
            return (a.numerator * b.denominator) <= (a.denominator * b.numerator);
        }

        // theo nguyên tắc, nếu override toán tử "==" thì đồng thời
        // cũng phải override phương thức Equals và GetHashCode
        // đề bài không y/c nhưng override để tránh warning.
        public override bool Equals(Object obj)
        {
            //Check for null and compare run-time types.
            if ((obj == null) || !this.GetType().Equals(obj.GetType()))
            {
                return false;
            }
            else
            {
                Fraction f = (Fraction) obj;
                return (this.numerator == f.numerator) && (this.denominator == f.denominator);
            }
        }

        public override int GetHashCode()
        {
            return (this.numerator << 2) ^ this.denominator;
        }


        // Bài tập vận dụng trang 18

        public static Fraction Add(Fraction f1, Fraction f2)
        {
            int numerator = f1.numerator * f2.denominator + f1.denominator * f2.numerator;
            int denominator = f1.denominator * f2.denominator;

            return new Fraction(numerator, denominator);

        }

        public static Fraction Subtract(Fraction f1, Fraction f2)
        {
            int numerator = f1.numerator * f2.denominator - f1.denominator * f2.numerator;
            int denominator = f1.denominator * f2.denominator;

            return new Fraction(numerator, denominator);
        }

        public static Fraction Multifly(Fraction f1, Fraction f2)
        {
            int numerator = f1.numerator * f2.numerator;
            int denominator = f1.denominator * f2.denominator;

            return new Fraction(numerator, denominator);
        }

        public static Fraction Divide(Fraction f1, Fraction f2)
        {
            int numerator = f1.numerator * f2.denominator;
            int denominator = f1.denominator * f2.numerator;

            return new Fraction(numerator, denominator);
        }
    }
}
