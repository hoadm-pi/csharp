﻿using System;

namespace E2
{
    class Program
    {
        static void Main(string[] args)
        {
            Fraction f1 = new Fraction(1, 2);
            Fraction f2 = new Fraction(2);
            Fraction f3 = new Fraction();

            // kiểm tra 3 constructors của Fraction
            Console.WriteLine(f1);
            Console.WriteLine(f2);
            Console.WriteLine(f3);

            // Phương thức rút gọn
            Console.WriteLine("---------------------");
            Fraction f4 = new Fraction(8, 12);
            f4.Simplify();
            Console.WriteLine(f4);

            // Kiểm tra các phương thức tính toán
            /*
             * 1/2 + 2/3 = 7/6
             * 1/2 - 2/3 =  -1/6
             * 1/2 * 2/3 = 2/6 
             * 1/2 / 2/3 = 3/4
             */
            //Console.WriteLine("---------------------");
            //Console.WriteLine("{0} + {1} = {2}", f1, f4, f1.Add(f4));
            //Console.WriteLine("{0} - {1} = {2}", f1, f4, f1.Subtract(f4));
            //Console.WriteLine("{0} * {1} = {2}", f1, f4, f1.Multifly(f4));
            //Console.WriteLine("{0} / {1} = {2}", f1, f4, f1.Divide(f4));

            // Kiểm tra các toán tử
            Console.WriteLine("---------------------");
            Console.WriteLine("-{0} = {1}", f1, -f1);
            Console.WriteLine("{0} + {1} = {2}", f1, f4, f1 + f4);
            Console.WriteLine("{0} - {1} = {2}", f1, f4, f1 - f4);
            Console.WriteLine("{0} * {1} = {2}", f1, f4, f1 * f4);
            Console.WriteLine("{0} / {1} = {2}", f1, f4, f1 / f4);

            // Kiểm tra các toán tử so sánh
            Console.WriteLine("---------------------");
            Console.WriteLine("{0} == {1} ({2})", f1, f2, f1 == f2);
            Console.WriteLine("{0} != {1} ({2})", f1, f2, f1 != f2);
            Console.WriteLine("{0} > {1} ({2})", f1, f2, f1 > f2);
            Console.WriteLine("{0} >= {1} ({2})", f1, f2, f1 >= f2);
            Console.WriteLine("{0} < {1} ({2})", f1, f2, f1 < f2);
            Console.WriteLine("{0} <= {1} ({2})", f1, f2, f1 <= f2);

            // Hỗn số

            // Khởi tạo hỗ số trực tiếp từ phân số
            MixedFraction mf1 = new MixedFraction(new Fraction(9, 2));

            // Khởi tạo với các thành phần
            MixedFraction mf2 = new MixedFraction(2, 2, 3);

            // Kiểm thử constructors vs ToString
            Console.WriteLine("---------------------");
            Console.WriteLine(mf1);
            Console.WriteLine(mf2);


            // Kiểm tra các toán tử với hỗ số 
            Console.WriteLine("---------------------");
            Console.WriteLine("-{0} = {1}", mf1, -mf1);
            Console.WriteLine("{0} + {1} = {2}", mf1, mf2, mf1 + mf2);
            Console.WriteLine("{0} - {1} = {2}", mf1, mf2, mf1 - mf2);
            Console.WriteLine("{0} * {1} = {2}", mf1, mf2, mf1 * mf2);
            Console.WriteLine("{0} / {1} = {2}", mf1, mf2, mf1 / mf2);


            // Sử dụng static
            Console.WriteLine("---------------------");
            Console.WriteLine("{0} + {1} = {2}", f1, f4, Fraction.Add(f1, f4));
            Console.WriteLine("{0} - {1} = {2}", f1, f4, Fraction.Subtract(f1, f4));
            Console.WriteLine("{0} * {1} = {2}", f1, f4, Fraction.Multifly(f1, f4));
            Console.WriteLine("{0} / {1} = {2}", f1, f4, Fraction.Divide(f1, f4));

        }
    }
}
