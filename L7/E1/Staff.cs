﻿using System;
namespace E1
{
    public class Staff:Person
    {
        private string department;
        private double salary;

        public Staff()
        {
        }

        public Staff(string name, int birthYear, string address, string department, double salary)
            :base(name, birthYear, address)
        {
            this.department = department;
            this.salary = salary;
        }

        public override void Input()
        {
            base.Input();

            Console.Write("Department: ");
            this.department = Console.ReadLine();

            Console.Write("Salary: ");
            this.salary = double.Parse(Console.ReadLine());
        }

        public override string ToString()
        {
            string baseInfo = base.ToString();

            return baseInfo + $"\nDepartment: {this.department}\nSalary: {this.salary}";
        }

        public void UpdateSalary(double salary)
        {
            this.salary = salary;
        }
    }
}
