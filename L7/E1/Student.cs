﻿using System;
namespace E1
{
    public class Student: Person
    {
        private string program;
        private int year;

        public Student() { }

        public Student(string name, int birthYear, string address, string program, int year)
            :base(name, birthYear, address)
        {
            this.program = program;
            this.year = year;
        }

        public override void Input()
        {
            base.Input();

            Console.Write("Program: ");
            this.program = Console.ReadLine();

            Console.Write("Year: ");
            this.year = int.Parse(Console.ReadLine());
        }

        public override string ToString()
        {
            string baseInfo = base.ToString();

            return baseInfo + $"\nProgram: {this.program}\nYear: {this.year}";
        }

        public void ChangeProgram(string program)
        {
            this.program = program;
        }
    }
}
