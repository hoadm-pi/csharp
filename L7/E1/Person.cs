﻿using System;
namespace E1
{
    public class Person
    {
        private string name;
        private int birthYear;
        private string address;

        public Person() { }

        public Person(string name, int birthYear, string address)
        {
            this.name = name;
            this.birthYear = birthYear;
            this.address = address;
        }

        public virtual void Input()
        {
            Console.Write("Name: ");
            this.name = Console.ReadLine();

            Console.Write("Birth Year: ");
            this.birthYear = int.Parse(Console.ReadLine());

            Console.Write("Address: ");
            this.address = Console.ReadLine();

        }

        public int getAge()
        {
            return DateTime.Now.Year - this.birthYear;
        }

        public override string ToString()
        {
            return $"Name: {this.name}\nBirth Year: {this.birthYear} ({this.getAge()})\nAddress: {this.address}";
        }
    }
}
