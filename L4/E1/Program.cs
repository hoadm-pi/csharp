﻿using System;

namespace E1
{
    class Program
    {
        static int counter = 1;
        /*
         * Bài tập 1 (slide bài giảng 4 - trang 15) 
         * 
         * Bài toán 8 Hậu
         * 
         * Viết thuật toán để in ra tất cả các nghiệm của bài toán 8 Hậu
         *
         */

        static int[,] CreateBoard(int n)
        {
            int[,] board = new int[n, n];

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    board[i, j] = 0;
                }
            }

            return board;
        }

        static void PrintSolution(int[,] board)
        {
            Console.WriteLine("Solution {0}: ", counter++);

            for (int i = 0; i < board.GetLength(0); i++)
            {
                for (int j = 0; j < board.GetLength(1); j++)
                {
                    Console.Write("{0} ", board[i, j]);
                }
                Console.WriteLine();
            }
            Console.WriteLine("------------------");
        }

        /*
         * Kiểm vị trí board[row, col] có an toàn không.
         * Do thuật toán sẽ đặt quân Hậu trên từng dòng, từ trái qua phải, do đó
         * trước khi đặt sẽ kiểm tra:
         *      - Cùng hàng
         *      - Chéo trên
         *      - Chéo dưới
         */
        static bool IsSafe(int[,] board, int row, int col)
        {
            int i, j;

            // Check this row
            for (i = 0; i < col; i++)
            {
                if (board[row, i] == 1)
                {
                    return false;
                }    
            }

            // Check upper diagonal
            for (i = row, j = col; i >= 0 && j >= 0; i--, j--)
            {
                if (board[i, j] == 1)
                {
                    return false;
                }
            }

            // Check lower diagonal 
            for (i = row, j = col; j >= 0 && i < board.GetLength(0); i++, j--)
            {
                if (board[i, j] == 1)
                {
                    return false;
                }    
            }    
                
            return true;
        }

        static bool Try(int[,] board, int col)
        {
            if (col >= board.GetLength(1))
            {
                // đặt đủ n quân hậu, in ra và reset lại bảng đánh dấu
                PrintSolution(board);
                return false;
            } else
            {
                for (int i = 0; i < board.GetLength(0); i++)
                {
                    if (IsSafe(board, i, col))
                    {
                        board[i, col] = 1;

                        if (Try(board, col + 1))
                        {
                            return true;
                        } else
                        {
                            // không có nghiệm ở bước n+1 -> xóa vết và quay lại
                            board[i, col] = 0;
                        }
                    }
                }
            }

            return false;
        }


        static void Main(string[] args)
        {
            /*
            n   solutions
            ------
            1   1
            2   0
            3   0
            4   2
            5   10
            6   4
            7   40
            8   92
            9   352
            10  724
            */
            int[,] board = CreateBoard(9);
            Try(board, 0);
        }
    }
}
