﻿using System;
using System.Linq;

namespace E3
{
    class Program
    {
        /*
        * Bài tập 3 (slide bài giảng 4 - trang 19) 
        * 
        * Chỉnh hợp không lặp: Chỉnh hợp không lặp chập k của tập 
        * n phần tử là một tập hợp gồm k phần tử được chọn từ n phần tử đã cho, 
        * có xét đến thứ tự và yêu cầu các phần tử được chọn phải khác nhau
        * 
        * Ví dụ tập {1,2,3} có các chỉnh hợp không lặp chập 2 là: {1,2},{1,3},
        * {2,1},{2,3},{3,1},{3,2}
        * 
        * Hãy xác định tất cả các chỉnh hợp không lặp chập k của tập
        * các số nguyên từ 1 đến n?
        *
        */

        static void PrintSolution(int[] a)
        {
            Console.Write("{");
            for (int i = 0; i < a.Length; i++)
            {
                if (i == 0)
                {
                    Console.Write("{0}", a[i]);
                }
                else
                {
                    Console.Write(", {0}", a[i]);
                }
            }
            Console.WriteLine("}");
        }

        static void Try(int[] arr, int[] data, bool[] status, int index, int r)
        {
            // đủ chuỗi, in rồi nghỉ
            if (index == r)
            {
                PrintSolution(data);
                return;
            }

            for (int i = 0; i < arr.Length; i++)
            {
                if (!status[i])
                {
                    data[index] = arr[i];
                    status[i] = true;
                    Try(arr, data, status, index + 1, r);
                    status[i] = false;
                }
            
            }
        }


        static public void Main()
        {
            //Console.Write("n: ");
            //int n = int.Parse(Console.ReadLine());
            //int[] arr = new int[n];
            //for (int i = 0; i < n; i++)
            //{
            //    arr[i] = i + 1;
            //}

            //Console.Write("r: ");
            //int r = int.Parse(Console.ReadLine());
            //int[] data = new int[r];
            //bool[] stt = new bool[n];

            //Try(arr, data, stt, 0, r);

        }
    }
}
