﻿using System;

namespace E2
{
    class Program
    {
        /*
         * Bài tập 2 (slide bài giảng 4 - trang 18) 
         * 
         * Chỉnh hợp lặp: Chỉnh hợp lặp chập k của tập n phần tử là một tập hợp
         * gồm k phần tử được chọn từ n phần tử đã cho, có xét đến thứ tự và
         * không yêu cầu các phần tử được chọn phải khác nhau
         * 
         * Ví dụ tập {1,2} có các chỉnh hợp lặp chập 2 là: {1,1}, {1,2}, {2,1}, {2,2}
         * 
         * Hãy xác định tất cả các chỉnh hợp lặp chập k của tập các 
         * số nguyên từ 1 đến n?
         *
         */

        static void PrintSolution(int[] a)
        {
            Console.Write("{");
            for (int i  = 0; i< a.Length; i++)
            {
                if (i == 0)
                {
                    Console.Write("{0}", a[i]);
                } else
                {
                    Console.Write(", {0}", a[i]);
                }
            }
            Console.WriteLine("}");
        }

        static void Try(int[] arr, int[] data, int index, int r)
        {
            // đủ chuỗi, in rồi nghỉ
            if (index == r)
            {
                PrintSolution(data);
                return;
            }

            for (int i = 0; i < arr.Length; i++)
            {
                data[index] = arr[i];
                Try(arr, data, index + 1, r);
            }
        }

       
        static public void Main()
        {
            Console.Write("n: ");
            int n = int.Parse(Console.ReadLine());
            int[] arr = new int[n];
            for (int i = 0; i <n; i++)
            {
                arr[i] = i+1;
            }

            Console.Write("r: ");
            int r = int.Parse(Console.ReadLine());
            int[] data = new int[r];


            Try(arr, data, 0, r);
        }
    }
}
