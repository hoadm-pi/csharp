﻿using System;

namespace E4
{
    class Program
    {
        /*
        * Bài tập 4 (slide bài giảng 4 - trang 19) 
        * 
        * Tổ hợp: Một tổ hợp chập k của n là một tập con k phần tử
        * không phân biệt thứ tự được chọn ra từ tập n phần tử.
        * 
        * Ví dụ tập {1,2,3,4} có các tổ hợp chập 2 là: {1,2},{1,3},{1,4},
        * {2,3},{2,4},{3,4}
        * 
        * Hãy xác định tất cả các tổ hợp chập k của tập các số nguyên từ 1 đến n?
        *
        *                                   1234
        *       1                       2                           3           
        *       2   3   4               3   4                       4
        */

        static void PrintSolution(int[] a)
        {
            Console.Write("{");
            for (int i = 0; i < a.Length; i++)
            {
                if (i == 0)
                {
                    Console.Write("{0}", a[i]);
                }
                else
                {
                    Console.Write(", {0}", a[i]);
                }
            }
            Console.WriteLine("}");
        }

        static void Try(int[] arr, int[] data, int start, int end, int index, int r)
        {
            if (index == r)
            {
                PrintSolution(data);
                return;
            }
          
            for (int i = start; i <= end && end - i + 1 >= r - index; i++)
            {
                data[index] = arr[i];
                Try(arr, data, i + 1, end, index + 1, r);
            }
        }

        
        static public void Main()
        {
            Console.Write("n: ");
            int n = int.Parse(Console.ReadLine());
            int[] arr = new int[n];
            for (int i = 0; i < n; i++)
            {
                arr[i] = i + 1;
            }

            Console.Write("r: ");
            int r = int.Parse(Console.ReadLine());
            int[] data = new int[r];

            Try(arr, data, 0, n - 1, 0, r);
        }
    }
}
